---
layout: post
title: Remove duplicates from an array
---

### Problem Statement
Given a sorted array of numbers, write a function that removes duplicates from
the array. The function receives an array, number of elements in the array and
should return a number, indicating how many elements in the array left after cleanup.

_Hint: You should be able to do it in-place without allocating an extra array._

### ...
