---
layout: post
title: Compute sum using bitwise operators
---
### TODO
  - problem statement
  - tools to be used for research
  - calculate `0b1 + 0b0` (how to)
  - explore carry-in/carry-out (how to)
  - calculate `0b1 + 0b1` (how to)
  - perf profile
  - practical application(s)
  - summary
  - fix custom lookable to work with signed numbers

### Problem Statement
Write a program that computes sum of two non-negative integers `a` and `b`. Keep in mind your code needs to meet the following constraints:
  - use only bitwise operators `<<`, `>>`, `|`, `&`, `~` and `^`;
  - use assignment operator `=`;
  - use equality checks and `boolean` combinations;

### Tools
Working with binary code is a lot of fun, however it also requires you to be very well aware of the actual bits being at work any time while your code is executing. After using good old `==` comparison operator to validate bit patterns in expressions:

```swift
0b1011 & 0b0110 == 0b0010 // true
```

Well, it's time for a better tool and here it is - `Binary` class that is actually a `CustomPlaygroundQuickLookable` and has guts to present any unsigned number as a bit pattern in Swift Playground with the desired precision (8-bit by default):

```swift
/// Custom helper to aid binary inspection of a given number
public class BitPattern: CustomPlaygroundQuickLookable {
    // initial state
    var bitPattern = 0, length = 0
    // our bit mask serving purpose cache key lookup function
    let cache_key = 0b11
    // our cache to perform lookups faster
    let cache = [0b00: "00", 0b01: "01", 0b10: "10", 0b11: "11"]

    // ctor
    init(bitPattern: UInt, length: Int) {
        self.bitPattern = Int(bitPattern: bitPattern)
        self.length = length
    }

    // custom quick look
    public var customPlaygroundQuickLook: PlaygroundQuickLook {
        var s = String(),
            temp_bits = bitPattern,
            bits_remaining = length
        // work on each pair of bits
        while temp_bits > 0 {
            // build the resulting string
            s = cache[temp_bits & cache_key]! + s
            // advance to the next pair
            temp_bits >>= 2
            bits_remaining -= 2
        }
        // fill-in the remaining bits with 0s
        s = String(repeating: "0", count: bits_remaining) + s
        // here is our final look
        return PlaygroundQuickLook.text(s)
    }

    /// Compares actual bit patterns.
    public static func == (l: BitPattern, r: BitPattern) -> Bool {
        return l.bitPattern == r.bitPattern
    }
}
```

To make it a no-brainer to use, we would wrap it into `bitPattern` extension of `Int` class from Swift's standard library:

```swift
extension Int {
    /// Bit pattern of an unsigned number with the given precision.
    /// Defaults to 8-bits and supports unsigned integers only.
    public var bitPattern: BitPattern {
        return BitPattern(bitPattern: UInt(self), length: 8)
    }
}
```

Here is a sneak peek of the tool at work while running experiments to find out the bug in the code (take a look at line #23 as an example):

![Custom Playground Quick Lookable for binary](/assets/custom-playground-quick-lookable.png)

Now, with the new shiny tool to tackle the challenge - lets do it!

### Confidence injection
Binary computation could be mind-bending at the first sight, however if you `tear it apart` and `begin with a simplest possible case` and `focus on one thing at a time` - your confidence and chances to solve it skyrocket.

### 1 + 0 = 1
As trivial as it sounds but our first case is to compute `1 + 0 = 1` expression using binary operators. To compute it lets put together a comparison chart that helps us to choose which bitwise operator to start with:

| `nickname` | `operator` | `expression` | `result` |
| __AND__ | __&__ | __1 & 0__ | __0__ |
| __OR__ | __\|__ | __1 \| 0__ | __1__ |
| __XOR__ | __^__ | __1 ^ 0__ | __1__ |

As you can see from the chart above, it is possible to use either `OR` or `XOR` operator to add `1` and `0` together. At this point it doesn't really matter which one to pick and actually if the initial choice will turn out to be wrong - it should be easy replace one with the other.

Lets pick `OR` to make it quick:

```swift
// 1 + 0 = 1
0b1 | 0b0 == 0b1 // (yay!)
```

### 1 + 1 = 0
Next case is to compute `1 + 1 = 0` expression a bit more involved but don't fret, it isn't a rocket science. To compute it lets put together yet another comparison chart to aid choosing bitwise operator to proceed with:

| `nickname` | `operator` | `expression` | `result` |
| __AND__ | __&__ | __1 & 1__ | __1__ |
| __OR__ | __\|__ | __1 \| 1__ | __1__ |
| __XOR__ | __^__ | __1 ^ 1__ | __0__ |

In this case it is a no-brainer which operator to pick, `XOR`, since `1 ^ 1 == 0` exactly what we need to compute the expression.

Let me point you to something interesting... To compute `1 + 0 = 0`, between `OR` and `XOR` we chose `OR` since both operators gave the same result. However, if we were to revert our initial pick to `XOR` - we would have only __1__ instead of __2__ operators to deal with, that would work wonders for both cases (hooray for code re-use!).

```swift
// 1 + 1 = 0
0b1 ^ 0b1 == 0b0 // (yay!)
// 1 + 0 = 1
0b1 ^ 0b0 == 0b1 // (yay again!)
```

### 1 + 1 = 0, carry = 1
Actually, in the previous section we intentionally omit the part which makes the simple computation slightly more complex and that is `carry`. When adding two `1s` to each other, we inevitably endup carrying out `1` to put into the next position.

To find out which operator will help us to compute `carry` lets take a look at the operators comparison chart again:

| `nickname` | `operator` | `expression` | `result` |
| __AND__ | __&__ | __1 & 1__ | __1__ |
| __OR__ | __\|__ | __1 \| 1__ | __1__ |
| __XOR__ | __^__ | __1 ^ 1__ | __0__ |

There are two operators `OR` and `AND` that yield the result that is `1` and looks like `carry` but are they equivalent? Lets find out by running two experiments using both operators and compare results:

```swift
// (A) 1 + 1 = 0, carry = 1
0b1 & 0b1 == 0b1 // OK
0b1 | 0b1 == 0b1 // OK

// (B) 1 + 0 = 1, carry = 0
0b1 & 0b0 = 0b0 // OK
0b1 | 0b0 = 0b1 // FAIL

// AND is the operator we should use :-)
```

Now it is clear which operator is the right choice to compute `carry`, since we must have a valid result for both cases `1 + 0 = 0, carry = 0` and `1 + 1 = 0, carry = 1`.

### Carry what?

However, there is one question left to answer about `carry`, how to make use of it? Luckily, the answer is easy, `carry` must always move to the next position with help of `<< (LEFT SHIFT)` operator:

```swift
// 1 + 0 = 1, carry = 0b00
(0b1 & 0b0) << 1 == 0b00 // OK
// 1 + 1 = 0, carry = 0b10
(0b1 & 0b1) << 1 == 0b10 // OK
```

Once we have `carry` and `sum` operands ready, the way to get our answer would be apply `OR` operator to both operands:

```swift
// 1 + 1 = 0, carry = 0b10
var sum = 0b1 ^ 0b1, carry = (0b1 & 0b1) << 1
// our answer, where 0b10 == 2
sum | carry == 0b10 // OK
```

Now toolset has tools to compute `sum` and `carry` and that is sufficient to compute very basic binary numbers, however we also need to develop a tool or two to be able add more complex numbers.

### 11 + 1 = 100
All the tools discovered above were concerned with the bits in `0` position but as we transition to bigger numbers, a new tool yet to be invented. As our numbers get bigger one thing remains constant, we can only add binary numbers bit-by-bit and that brings us some challenges and here they are:

 - keep track of the current position;
 - gracefully move between bits;
 - keep track of the carry position;

```swift
// to track the current position - use bit mask,
// starting with bit at position 0

```



### Tools in action
...

### Perf profile
...

### Practical application(s)
...

### Summary
...