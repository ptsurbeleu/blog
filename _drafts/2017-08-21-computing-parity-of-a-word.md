---
layout: post
title: Computing the parity of a word
---

### Problem Statement
The parity of a binary word is 1 if the number of 1s in the word is odd; otherwise, it is 0. For example, the parity of 1011 is 1, and the parity of 10001000 is 0. Parity checks are used to detect single bit errors in data storage and communication. It is fairly straightforward to write code that computes the parity of a single 64-bit word.

How would you compute the parity of a very large number of 64-bit words?

_Hint: Use a lookup table, but don't use 2^64 entries!_

### Compute parity (brute-force)
Compute parity using brute-force algorithm is fairly straightforward. Here is how you would do it in Swift:

{% highlight swift %}
// we use 64-bit unsigned integers
func parity_v1(x: UInt64) -> UInt64 {
    var result: UInt64 = 0, word = x
    while word != 0 {
        result += (word & 1)
        word >>= 1
    }
    return result % 2
}

// validate the function works as advertised
parity_v1(x: 0b0000) == 0
parity_v1(x: 0b0010) == 1
parity_v1(x: 0b0011) == 0

{% endhighlight %}

Time complexity of this code snippet is _O(n)_, where _n_ is the number of bits in the given word _(64 in this case)_.

### Compute parity (slightly faster)
However, there is a slightly faster way to perform the very same computation with a technique erasing the lowest set bit in a word using a single operation:

{% highlight swift %}

// erasing the lowest bit set technique for 0b0101_0001
// x &= (x - 1)

// iteration 0
0b0101_0001 - 1           == 0b0101_0000 // x - 1
0b0101_0001 & 0b0101_0000 == 0b0101_0000 // x & (x - 1)

// iteration 1
0b0101_0000 - 1           == 0b0100_1111 // x - 1
0b0101_0000 & 0b0100_1111 == 0b0100_0000 // x & (x - 1)

// iteration 2
0b0100_0000 - 1           == 0b0011_1111 // x - 1
0b0100_0000 & 0b0011_1111 == 0b0000_0000 // x & (x - 1)

// no more iterations, since the last resulted in 0

{% endhighlight %}

Here is the actual code snippet using the technique described above:

{% highlight swift %}

// we use 64-bit unsigned integers
func parity_v2(x: UInt64) -> UInt64 {
    var result: UInt64 = 0, word = x
    while word != 0 {
        // exploit XOR properties, where
        // 0b0000 ^ 1 == 1, 0b0001 ^ 1 == 0 and etc.
        // essentialy we flip the first bit back & forth
        // just like this...
        // on 1st iteration 0b0000 ^ 1 = 0b0001
        // on 2nd iteration 0b0001 ^ 1 = 0b0000
        result ^= 1
        // erase the lowest set bit
        word &= (word - 1)
    }
    return result
}

parity_v2(x: 0b0000) == 0
parity_v2(x: 0b0010) == 1
parity_v2(x: 0b0011) == 0

{% endhighlight %}

Time complexity of this code snippet is _O(k)_, where _k_ is the number of bits set in the word. For example, _0b0101_0100, k = 3_.

### Compute parity (large number of words)
The other constraint is a very large number of words and there are two key techniques
to performance, processing multiple bits at a time and caching results in an array-based
lookup table.

First, lets take a look how the caching technique works. Here is an example code snippet
outlining an array-based parity lookup table for 4-bit words that nicely fits in memory,
since it requires only 16 _(2 ^ 4)_ array entries:

{% highlight swift %}

// array-based lookup table definition, where index is a lookup key
var cache = [UInt]()
// manually compute parity for all 4-bits words
cache[0b0000] = 0 // where 0b0000 = 0
cache[0b0001] = 1 // where 0b0001 = 1
...
cache[0b0111] = 1 // where 0b0111 = 7
cache[0b1000] = 1 // where 0b1000 = 8
...
cache[0b1110] = 1 // where 0b1110 = 14
cache[0b1111] = 0 // where 0b1111 = 15

{% endhighlight %}

Here is the actual code snippet using caching technique described above:

{% highlight swift %}

func parity_v3(x: UInt64) -> UInt {
    // precomputed parity cache of all 4-bit words
    let cache: [UInt] = [0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0]
    // our 4-bit word mask
    let mask: UInt64 = 0xf
    // our 4-bit blocks, int conversion is a must
    // because array indices are signed integers
    let _1st = Int((x >>  0) & mask)
    let _2nd = Int((x >>  4) & mask)
    let _3rd = Int((x >>  8) & mask)
    let _4th = Int((x >> 12) & mask) 
    // compute parity by XORing all blocks
    return cache[_1st] ^ cache[_2nd] ^ cache[_3rd] ^ cache[_4th]
}

// 4-bit words
parity_v3(x: 0b0000) == 0 // true
parity_v3(x: 0b0001) == 1 // true
parity_v3(x: 0b0010) == 1 // true
parity_v3(x: 0b1110) == 1 // true

{% endhighlight %}

Key takeaways:

 - y = x & ~(x - 1);
 - To improve performance, manipulate multiple bits at the same time;
 - To handle large amounts of input data, use array-based lookup table;

Codify your takeaways...

### y = x & ~(x - 1)

### Manipulating multiple bits at the same time
XOR(^), AND(&), OR (|)

### Array-based lookup table (bit manipulations)
This is an example of a hand-crafted array-based lookup table of 4-bit words in Swift:

let parityCache: [Int] = [0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0]

How would you reason about it? Since we defined our word has 4 bits, we need to generate 2 ^ 4 (16) unique sequences and bit pattern for each item in the lookup table actually corresponds to actual numbers from 0 to 15.

Here is how we do it by hands:

 - 0 is 0b0000_0000 and its parity 0
 - 1 is 0b0000_0001 and its parity 1
 - 2 is 0b0000_0010 and its parity 1
 - 3 is 0b0000_0011 and its parity 0
 - 4 is 0b0000_0100 and its parity 1
 - 5 is 0b0000_0101 and its parity 0
 - 6 is 0b0000_0110 and its parity 0


Found a true gem of bit twiddling & hacks from Stanford U here: https://graphics.stanford.edu/~seander/bithacks.html