---
layout: post
title: DNS & Hosts & Rock and Roll 
---
Armed with common prejuidices

Apple Technical Q&A QA1339 (https://developer.apple.com/library/content/qa/qa1339/_index.html)
Q: Is it possible to retrieve information regarding the current internal state of mDNSResponder?

sudo killall -info mDNSResponder (Console app, /var/log/system.log and then filter by mDNSResponder)

Look up /etc/hosts section there to find out what actually your mDNSResponder sees.

man mDNSResponder - has lots of very useful hints and tricks (including USR1 and USR2 signals).

dns-sd -q name rrtype rrclass

look up any DNS name, resource record type, and resource record class, not necessarily DNS-SD names and record types. If rrtype is not specified, it queries for the IPv4 address of the name, if rrclass is not specified, IN class is assumed. If the name is not a fully qualified domain name, then search domains may be appended.

dns-sd -q avocado.pabloduo.com ~> to lookup current state of resolver (has some glitches when you switch back & forth between IP)

ended up writing into '/private/etc/hosts' to avoid messing with '/etc/hosts' file

To let mDNSResponder know your changes should be processed, do write simple comments into the file. In my case
I did it by simply writing an empty section to the file, otherwise mDNSResponder will not evict your '127'
records from its cache.