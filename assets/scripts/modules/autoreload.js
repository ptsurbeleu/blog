import u from "../umbrella/module.js"

// Very basic module to auto-refresh current page on changes (if any)
(async (document,u) => {
  // Polls status of '/uuid' endpoint to reload page on changes (if any)
  async function pollStatus() {
    // Fetch the latest timestamp from the server
    let latest = await timestamp();
    // Looks like the page we are currently viewing is outdated
    if (latest !== original) {
      // Keep track of the mutated state
      console.log(`${original} !== ${latest}, hence forcing to reload...`);
      // Force current document to reload
      location.reload();
      // Just exit
      return;
    }
    // Keep track of the current state
    console.log(`${original} === ${latest}, no changes have been found...`);
  }

  // Simple helper to query timestamp from the server
  async function timestamp() {
    // Fetches /uuid endpoint
    let uuid = await fetch("/uuid", {cache: "no-store"})
        .then(x => x.ok ? x.json() : null)
        .catch(_ => console.log("oops, something went wrong..."));
    // Here is the timestamp
    return uuid ? uuid.timestamp : original;
  }

  // Cache original timestamp and location object
  let original = await timestamp(), location = document.location;
  // Start polling
  setInterval(pollStatus, 5000);
})(document, u);