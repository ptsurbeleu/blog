import u from "../umbrella/module.js"

(function(document, u) {
  "use strict";

  // Toggles search dialog
  function toggle(e) {
    u('.search-overlay').toggleClass("search-overlay--open");
    u('.search-overlay__input').focus().clear();
    e.preventDefault();
  }

  function keypress(e) {
    // ensure it is pure `~` without Cmd (for OSX)
    if (e.charCode !== 96 || e.metaKey) return;
    toggle(e);
  }

  // Hookup elements & events with handlers
  u('body').on('keypress', keypress);
  u('.search-toggle').on('click', toggle);
})(this, u);