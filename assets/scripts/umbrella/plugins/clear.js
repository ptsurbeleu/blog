import u from "../umbrella.js"

// Clears element's value
u.prototype.clear = function() {
  this.first().value = '';
  return this;
};