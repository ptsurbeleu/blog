import u from "../umbrella.js"

// Sets focus on the specified element
u.prototype.focus = function() {
  this.first().focus();
  return this;
};