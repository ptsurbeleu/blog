import u from "../umbrella.js"
/// ...
u.prototype.next = function() {
  return this.map(e => e.nextElementSibling).coalesce(null);
}

export default {};