import u from "../umbrella.js"
/// ...
u.prototype.previous = function() {
  return this.map(e => e.previousElementSibling).coalesce(null);
}
/// ...
export default {};