---
layout: post
title: Balanced binary tree (v2)
---

In the [previous (recursive 2x) version]({% link _posts/2017-09-23-balanced-binary-tree-v1.md %}) of the code snippet,
we have been able to compute answer to the problem with time complexity of __O(N^2)__. Lets see
if there is a faster way to perform the computation.

As we discovered, our main function `balanced`, calls to `height` function to calculate height of an __every subtree__ on an __every iteration__ to find out whether subtrees differ by more than one.

If we moved the decision making code from `balanced` into `height` function and terminated the recursion early (_case of not balanced tree_) it would save us some cycles. We can compute our answer faster by introducing a __sentinel value__ to signal when subtree is not balanced, while calculating height of the subtree...

That is an interesting idea, but how that would work? It turns out quite neatly:

 - If subtrees differ by 0 or 1, __return larger of heights + 1__ (_nothing new_);
 - If subtrees differ by more than 1, __return -1__ (_our sentinel value_);

Lets see how an improved version of the code snippet would look like:
```swift
// Tree node definition (recursive data structure).
class Node {
    // Data, left and right nodes
    var data: String = "", left: Node?, right: Node?
    
}

// Helper function to calculate height of any given tree.
func height(node: Node?) -> Int {
    // base case
    if node == nil { return 0 }
    // calcuate left subtree height
    let left = height(node: node?.left)
    // terminate early, since left subtree is not balanced
    if left == -1 { return -1 }
    // calculate right subtree height
    let right = height(node: node?.right)
    // terminate early, since right subtree is not balanced
    if right == -1 { return -1 }
    // calculate the absolute difference between subtrees
    // ... and terminate early when subtree is not balanced
    if abs(left - right) > 1 { return -1 }
    // calculate the subtree height on the current level
    return max(left, right) + 1
}

// Main function to solve the problem
func balanced(tree: Node?) -> Bool {
    // calculate height of the entire tree and remember,
    // height would be -1 if the tree is not balanced.
    return height(node: tree) != -1
}

// validations
// build a 2x2 tree (balanced)
let twoByTwo = withHeight(n: 2, m: 2)
// assert the tree is balanced, as expected
balanced(tree: twoByTwo) == true // true

// build a 3x2 tree (still balanced)
let threeByTwo = withHeight(n: 3, m: 2)
// assert the tree still balanced, as expected
balanced(tree: threeByTwo) == true // true

// build 5x3 tree (unbalanced)
let fiveByThree = withHeight(n: 5, m: 3)
// assert the tree is unbalanced, as expected
balanced(tree: fiveByThree) == false // true
```

Did we actually improve anything? It turns out that yes, we did. Our time complexity for this code snippet is __O(N)__ compared to __O(N^2)__ previously, where __N__ is the number of nodes in a tree.