---
layout: post
title: Find route between nodes in graph (DFS)
---
Our [previous version]({% link _posts/2017-10-23-digraph-find-route-between-nodes-bfs.md %}) of the problem solution was using __breadth-first search__ approach to find the route between two nodes. This time we will be exploring __depth-first search__, what it is good for and its performance profile.

Here is the functional specification of the algorithm in Wikipedia: *__depth-first search__ is an algorithm for traversing or searching tree or graph data structures. One starts at the root (selecting some arbitrary node as the root in the case of a graph) and explores as far as possible along each branch before backtracking.*

It turns out that a French matematician Charles Pierre Tremaux was investigating a version of depth-first search as a strategy for solving mazes in 19th century.

Here is our directed graph interface (_using adjacency list representation_):

```swift
class DirectedGraph {
  // ctor
  init(numberOfVertices: Int)
  // adds an edge between vertices in graph
  addEdge(v: Int, w: Int)
  // gets adjacent vertices or empty array
  adj_vertices(v: Int) -> [Int]
  // marks the given vertice as visited
  visit(v: Int)
  // find out whether the vertice is visited or not
  visited(v: Int) -> Bool
}
```

Here is the function definition:

```swift
func routable(graph: DirectedGraph, x: Int, y: Int) -> Bool {
  // implement me...
}
```

### Branstorming
There is not that much for us to brainstorm about, since we already have a working solution that uses __breadth-first search__ and we can reuse the code as is and replacing only parts relevant to the search technique itself. In this case we will be using a non-recursive implementation and its core component `Stack` data structure.

Here is the data structure specification (_using linked-list representation_):
```swift
public class Stack<T> {
    // ctor
    public init ()
    // pushes an item onto the stack
    public func push(item: T)
    // is the stack empty or not
    public func empty() -> Bool
    // pops an item from the stack
    public func pop() -> T?
}
```

### Solution
In this case we will be using __depth-first search__ technique to find the route between two nodes in a graph. We begin traversing the graph at node `x` and work our way through all adjacent nodes of `x` and their adjacent nodes. Along the way, we verify whether the value of nodes we encounter equals `y` and when it does that means we just found the route from `x` to `y`.

And we already have implemented a way to track nodes visited already, so no need to spend time on that.

### Depth First Search
One of the very distinct properties of the technique is that we traverse the adjacent node very deeply before inspecting the immediate child nodes.

As we already stated above, our core component empowering the search is `Stack` helping us to keep track of the nodes to inspect and we push `x` node to initiate the inspection of the graph. Our next step, is to immediately pop `x` node from the stack and enumerate its adjacent nodes while at the same comparing each node with `y` and breaking out of the loop once the adjacent child node value matches `x`. In case the value of the child node doesn't match `y`, that child node is pushed onto the stack with a hope that one of its adjacent nodes (if any) would match `y`'s value.

Again, this technique would traverse the graph either finding a node matching `y`'s value and returning `true` or returning `false` if none of the nodes match.

Here is our implemenation of the algorithm using `Stack` data structure:
```swift
func routable(graph: DirectedGraph, x: Int, y: Int) -> Bool {
    // storage to keep track of nodes to be visited
    let stack = Stack<Int>()
    // start with our first node x
    stack.push(item: x)
    // fetch cursor to iterate on
    while let cursor = stack.pop() {
        // enumerate all adjacent vertices
        for n in graph.adj_vertices(v: cursor) {
            // skip node we already visited
            if graph.visited(v: n) { continue }
            // yay, we have found a route between nodes!
            if n == y { return true }
            // keep looking, thus push the node onto the stack
            stack.push(item: n)
        }
        // mark our cursor node as visited
        graph.visit(v: cursor)
    }
    // no path has been found between nodes
    return false
}
```

Here we will be reusing the `withEdges` helper utility from __breadth-first search__ solution to validate this implementation. Here is the assertion snippet validating `routable` function from above works as expected:
```swift
// very simple graph definition, 2 -> 0 -> 1
let graphOne = withEdges((2,0), (0,1))

// validation
routable(graph: graphOne, x: 2, y: 1) == true
```
This implementation of `routable` function has __O(V + E)__ time complexity, where `V` number of vertices and `E` number of edges in the directed graph.