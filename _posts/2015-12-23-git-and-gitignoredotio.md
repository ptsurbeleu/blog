---
layout: post
title: Git is even better with gitignore.io
---

Maintaining a project's repository in good shape is a task of the same importance as creating quality content for the project needs. In some source control systems it is a developer's resposibility to ensure no unwanted files are checked-in with the upcoming commit and others might have tools dedicated just for that purpose.

Lets talk about what Git has to offer in this case.

Git has [.gitignore](http://bit.ly/22mAgOn) file to shield a developer from accidentally sending unwanted files or build artefacts to the repository with a commit. While *.gitignore* aims to blacklist unwanted files or artefacts and keep the repository tidy, there is a caveat associated with it. Since all the files by default are whitelisted (even the ones you don't want to have in the repository) - it is your responsibility to configure the repository and make Git aware which files should be ignored.

And in some cases, you may memorize locations, file extensions and artefact names that shouldn't be committed to the repository but oftentimes it takes trial and error to get it right.

This is where [gitignore.io](http://bit.ly/1mfrK2O) comes to the rescue helping you to get it just perfect in virtually no time right from your browser (or your terminal) window.

Here is an example of how would you generate *.gitignore* file to ignore Xcode and OSX artefacts using the browser:

![Ignore Xcode and OSX artefacts](/assets/gitignore.io.png)

Wow, it is simply amazing! Folks have put it all together so you can just query for the exact list of things to ignore using their website - pure awesomeness.

Hold on, what that [Command Line Docs](http://bit.ly/1mfryAJ) link is? Is that possible to query for a new *.gitignore* file with the desired things to be ignored, like *osx, xcode* and etc. right there in terminal? Exactly, here is how you define a corresponding __gi__ function in your [.bash_profile](http://bit.ly/1JIX9VI):

{% highlight bash %}

# gitignore.io
function gi() {
  curl -L -s https://www.gitignore.io/api/$@
}

{% endhighlight %}

The idea is so lovely, since it neatly optimizes Git experience and should become a part of my bash profile gems collection.

And here goes an example of using __gi__ function helper in the terminal window to ignore Xcode and OSX artefacts:

{% highlight bash %}

$ gi xcode,osx

# Created by https://www.gitignore.io/api/xcode,osx

### Xcode ###
# Xcode
#
# gitignore contributors: remember to update Global/Xcode.gitignore, Objective-C.gitignore & Swift.gitignore

## Build generated
build/
DerivedData

## Various settings
*.pbxuser
!default.pbxuser
*.mode1v3
!default.mode1v3
*.mode2v3
!default.mode2v3
*.perspectivev3
!default.perspectivev3
xcuserdata

## Other
*.xccheckout
*.moved-aside
*.xcuserstate


### OSX ###
.DS_Store
.AppleDouble
.LSOverride

...

{% endhighlight %}

Hope you will find these hints useful.
