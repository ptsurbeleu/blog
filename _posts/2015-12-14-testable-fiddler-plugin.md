---
layout: post
title: Testable Fiddler Plugin
---

If you do web development, chances are you use [Fiddler](http://bit.ly/1XX2AC6) on a regular basis. Fiddler is a well-crafted tool which has lots of built-in features that can drastically improve your productivity.

However, as with any tool there are use case(s) in your expertise domain that are beyond the built-in capabilities of the tool. Fortunately, Fiddler has a built-in extensibility model to empower developers with a way enhance many (if not all) of its aspects, such as custom inspectors, QuickExec handlers and etc. (here you can find all about it - [Debugging with Fiddler](http://amzn.to/1OLSuU8)).

One of my colleagues at Microsoft, [Vittorio Bertocci](http://bit.ly/1OLSDH4) and his team, run
[Azure Active Directory](http://bit.ly/1OLT0RX) (also known as AAD, for short),
maintain [SDK, samples + tools](http://bit.ly/1OLSV0F) and help their customers with troubleshooting
when things don't work the way they should. One of the tasks they tackle with during troubleshooting is the task of decoding
content of bearer JWT tokens.

Since Vittorio's team uses Fiddler a lot on a daily basis and the intent to keep
using the already familiar tool dominated... Vittorio came up with an idea to develop
a custom Fiddler plugin for that matter and I was more than happy to volunteer for the project [OInspector](http://bit.ly/1LRwLKp).

Already having a few Fiddler plugins under my belt, I decided that with this new
plugin its testability should be done better. The very first plugin was a lot of
fun to develop but very painful to validate. Here is why - build sources, shutdown
Fiddler, copy binaries to the destination, generate the desired session for inspection,
only after that I was able to get to the point where the plugin can be observed at
work (oftentimes I observed a crash actually). The development of the second plugin
was a bit better (literally, just a bit) - I managed to optimize friction to shutdown
Fiddler, copy freshly built plugin binaries to the destination
(thanks to [Post-Build Event](http://bit.ly/1XV8kfs) feature in Visual Studio)

{% highlight batch %}

copy "$(TargetDir)*.*" "%ProgramFiles(x86)%\Fiddler2\Inspectors\*.*"

{% endhighlight %}

and used ad hoc response/request strings to build a corresponding Session object but it turns out the task to re-generate actual Session for the plugin to
work with was still a very big productivity hit. And sometimes, you really have to go far and beyond to be able to generate the actual Session with the
data your plugin expects to process.

With OInspector plugin one of the first priority decisions made was to spend some time investigating if it is a feasible task to perform unit test runaways
using actual Sessions captured & saved in [SAZ format](http://bit.ly/1XV7Y8N). It was quite a surprise to discover that Xceed
component which Fiddler uses to handle SAZ format has licensing checks in place prohibiting its use in a context other than Fiddler runtime.

Trying this and that way, it seemed to be impossible to use sessions in SAZ format for testing, since Xceed component requires a license and I don’t have
any for that matter. But… before giving it up I decided to take a closer look at the component using [Reflector](http://bit.ly/1OLTvvl) by RedGate to
peek into Xceed's component internals and after some back and forth navigation thru the component's code I found a very interesting snippet that was the last
piece of the puzzle. It was a discovery of LicenseManager. Unfortunately, LicenseManager does not provide a license context for testing runtime but there is a
license context for design time [DesigntimeLicenseContext](http://bit.ly/1Iz44Ax). Yay! That was exactly what I was looking for...

{% highlight csharp %}

[AssemblyInitialize]
public static void GlobalInitialize(TestContext testContext)
{
    // Enables us to use Xceed's component to read SAZ files
    LicenseManager.CurrentContext = new DesigntimeLicenseContext();
}

{% endhighlight %}

Here is the link to the original source that makes use of DesigntimeLicenseContext: [TestBootstrap.cs](http://bit.ly/1OLWCTS)

After this enhancement in OInspector's unit tests project, it became possible to use a captured OpenID Connect session with the actual content and then running
it (dozen) times thru the plugin's code via MSTest + Visual Studio.

{% highlight csharp %}

[TestMethod]
public void ScoreSessionWithCapturedFiddlerSessionsArchive()
{
    // Arrange
    var expected = 99;

    // Act
    var actual = this.Act(@".\testSamples\oidc-authorization-code-response-cc.saz");

    // Assert
    Assert.AreEqual(expected, actual);
}

private int Act(string testSample)
{
    var inspector = new TestableResponseInspector();
    return inspector.ScoreForSession(testSample);
}

{% endhighlight %}

It made possible to write code that was under low-level scrutiny of the actual content from
the session captured. All issues, features or improvements can be implemented and validated without even opening Fiddler.

This __test first__ quality has already saved hours and hours of tedious manipulations and unnecessary overhead. Need to repro an issue with the plugin? Easy,
just capture the session in Fiddler that reproduces the problem and then use it as a payload for the test coverage to identify the problem and fix the code.

Done.


__P.S.__ *Special thank you goes to [Vittorio](http://bit.ly/1R5SLmj) for the opportunity to contribute to the project, since he was the mastermind behind the idea of OInspector.*

__P.P.S.__ *One of the good reads I found to be very inspirational on the subject of testable code is [Professional Test Driven Development with C#: Developing Real World Applications with TDD](http://amzn.to/1MBiRpU) by James Bender and Jeff McWherter. It has been an invaluable source of knowledge and field wisdom for me, since the day I finished the first chapter, I strive not to write
a single line of code without writing a test for it in the first place. Though, writing testable code is a challenging task it pays off in many ways in the long run. As authors put it in the book, if you have no tests for the code you just wrote then you have no reliable way to prove your code does what you claim it is supposed to.*

Sources and materials used to prepare this post:

- [Debugging with Fiddler: The complete reference from the creator of the Fiddler Web Debugger](http://amzn.to/1IhPQV3) by [Eric Lawrence](http://bit.ly/1MppP0J)
- [MSDN: Pre-build Event/Post-build Event Command Line Dialog Box](http://bit.ly/1P3aXK7)
- [MSDN: Recognized Environment Variables](http://bit.ly/1QoMFxj)
- [Pygments: Available lexers](http://bit.ly/1QoN0js)
- [Bitbucket: Fiddler Plugins](http://bit.ly/1QoNvdv)
- [Github: OInspector](http://bit.ly/1LRwLKp)
- [Github: Markdown Basics](http://bit.ly/1QoNR3O)
