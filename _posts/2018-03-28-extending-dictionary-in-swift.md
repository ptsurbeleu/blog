---
layout: post
title: Extending Dictionary in Swift
---
Have got an interesting case while porting a code snippet from **Java** to **Swift**, where **Swift**'s documentation was failing me, StackOverflow haven't heard about the problem and DuckDuckGo search results were vague as well.

Here is the original snippet in **Java**:
```java
Map<Integer, Integer> parent = new HashMap();
...
if (parent.containsKey(edge[1])) {
  ...
} else {
  ...
}
```
`HashMap` in **Java** is similar to `Dictionary` in **Swift**, however, in **Swift** there is no `containsKey` and as usually, this calls for an extension.

Here is the first attempt to write an extension:
```swift
extension Dictionary {
  func containsKey(key: Int) -> Bool {
    if let _ = self[key] {
      return true
    }
    return false
  }
}
```
... and that failed miserably, with a very obscure compiler error that took me some time to figure out:

![Swift compiler error](/assets/20180328/swift-compiler-error.png)

After going thru countless number of posts and search results... I figured it out from my **C#** background, that `generics are generics` and should behave as such regardless of the language.

**Swift** probably follows the same convention, such as `Dictionary<TKey, TValue>` and therefore `key` argument must be of type `TKey` (actually just `Key` in **Swift**):
```swift
extension Dictionary {
    // Magic trick here is to use type "Key"
    func containsKey(key: Key) -> Bool {
        // "let" helps to determine if the key is present 
        if let _ = self[key] {
            // Yes, key is found
            return true
        }
        // No, key is not found
        return false
    }
}
```
Hope that hint would save some hassle next time you decide to write an extension for **Swift**'s `Dictionary`.

Let me know.