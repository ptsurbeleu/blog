---
title: Delete duplicates from an array
---
### Problem statement
Write a function that removes duplicates from a sorted array, compacts the remaining elements and fills empty spaces with 0s.

```
EXAMPLE:

  Input: n = [2,3,3,4,5,5]

  Output: [2,3,4,5,0,0]
```

### Solution
Here is the actual code of the algorithm that deletes duplicates from a sorted array, compacts remaining elements and fills empty stapes with `0`s:
```swift
func rm(n: [Int]) -> [Int] {
    // Prepare state of the algorithm
    var seq = n, read = 0, write = 0, key = 0
    // Keep processing unless reading head is out of bounds
    while read < n.count {
        // Match reading head with the key from the last iteration
        if seq[read] == key {
            // Set to 0 value of the head's element 
            // and advance to the next position
            seq[read] = 0; read += 1
            // Skip to the next processing cycle
            continue
        }
        // Re-discover the key used as a marker to find duplicates
        key = seq[read]
        // Mismatch between writing & reading heads is a signal to swap
        if write != read { swap(&seq, write, read) }
        // Move both writing & reading heads to their next elements
        read += 1; write += 1
    }
    // Here is the answer
    return seq
}
```

Following is the traditional section with assertions to validate the code performs as advertised and establish a baseline for any improvement work in the future:

![Delete duplciates from a sorted array test assertions](/assets/20180909/test_assertions.png)

### Bits & pieces
This coding challenge is one of many variations on the subject of manipuating array entries in an efficient way. It may take a couple attempts to wrap your head around the parts of the algorithm but you should be able to master the technique quickly.

Lets get started!

The first statement prepares the state of the algorithm and declares a few variables, the most interesting are `read` and `write` that define __reading head__ and __writing head__ heads, and `key` that defines a duplicate key to match with at every iteration:
```swift
// Prepare state of the algorithm
var seq = n, read = 0, write = 0, key = 0
```

Next statement is the __main loop__ executed until `read` _(eq. __reading head__)_ reaches the end of the array, which indicates the processing has been completed:
```swift
// Keep processing unless reading head is out of bounds
while read < n.count {
    ... inner block of code ...
}
```

Following `if` block, the first block in the loop, takes care of the case matching __reading head__'s value with the key considered to be a duplicate. Once the match is found, this block resets the element's value to `0`, advances __reading head__ to the next position and skips to the next processing cycle:
```swift
// Match reading head with the key from the last iteration
if seq[read] == key {
    // Set to 0 value of the head's element 
    // and advance to the next position
    seq[read] = 0; read += 1
    // Skip to the next processing cycle
    continue
}
```

This statement is quite simple but nontheless a very important part of the algorithm that re-discovers the key used as a marker to find duplicates at the next iteration:
```swift
// Re-discover the key used as a marker to find duplicates
key = seq[read]
```

Following the key discovery block is the statement to compact the sequence being processed with a conditional swap between __reading__ and __writing__ heads. Note however, the compaction step is applicable only when position of both heads is out of sync:
```swift
// Mismatch between writing & reading heads is a signal to swap
if write != read { swap(&seq, write, read) }
```

Next statement is a must one to have that advances __writing__ and __reading__ heads to the elements in the next position, since the algorithm needs to keep it going:
```swift
// Move both writing & reading heads to their next elements
read += 1; write += 1
```

As soon as the __main loop__ is over, the code is ready to return the sequence that has duplicates removed, compacted remaining elements and filled in empty spaces with `0`'s at the end:
```swift
// Here is the answer
return seq
```

That's all folks! 🐯

Here is a Swift playground for this article at Github: [Delete duplicates from an array](http://bit.ly/2Mhgno0).