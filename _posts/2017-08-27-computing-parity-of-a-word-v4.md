---
layout: post
title: Computing the parity of a word (v4)
---

The [previous (large number of words) version]({% link _posts/2017-08-26-computing-parity-of-a-word-v3.md %}) of the code snippet addressed remaning concerns and armed with v3 insights,
we can roll out even better way to perform the computation.

### Compute parity (faster and smarter)
Here is our v3 insight we are going to build upon: the XOR of two bits is 0 if both
bits are 0 or 1, if the bits differ it is 1.

{% highlight swift %}

// XOR truth table
0 ^ 0 == 0 // true
1 ^ 1 == 0 // true
0 ^ 1 == 1 // true
1 ^ 0 == 1 // true

{% endhighlight %}

One of the XOR properties is being commutative, which means the order in which XORs
are performed does not change the outcome. On the other hand, XOR of a group of bits
is its parity and we can exploit this fact to use word-level XOR instruction to process
multiple bits at a time.

Lets work on computing parity manually using two 8-bit words, such as _0b0010_1100 (odd)_
and _0b0010_0100 (even)_:

{% highlight swift %}
// manually compute parity for 0b0010_1100 (odd)
// by using shift and XOR operators

// x ^ (x >> 4), 4 bit words
0b0010_1100 ^ 0b0000_0010 == 0b0010_1110 // true
// x ^ (x >> 2), 2 bit words
0b0010_1110 ^ 0b0000_1011 == 0b0010_0101 // true
// x ^ (x >> 1), 1 bit words
0b0010_0101 ^ 0b0001_0010 == 0b0011_0111 // true
// but we need either 0 or 1... therefore, use AND!
0b0011_0111 & 1 == 1 // true, just as expected


// manually compute parity for 0b0010_0100 (even)
// by using shift and XOR operators

// x ^ (x >> 4), 4 bit words
0b0010_0100 ^ 0b0000_0010 == 0b0010_0110 // true
// x ^ (x >> 2), 2 bit words
0b0010_0110 ^ 0b0000_1001 == 0b0010_1111 // true
// x ^ (x >> 1), 1 bit words
0b0010_1111 ^ 0b0001_0111 == 0b0011_1000 // true
// but we need either 0 or 1... therefore, use AND!
0b0011_1000 & 1 == 0 // true, just as expected

{% endhighlight %}

By closely observing raw binary representation of these computations, you
might notice a pattern - the word size is cut by half with each shift and
that is because our __"comparable"__ word size is also shrinking with each
iteration, while we also ignoring the most significant bits following the
__"comparable"__ word size. After the last shift operation _(x >> 1)_, the least
significant bit _(LSB)_ carries our word parity, either 0 or 1.

Since the final result cannot be used as is, we simply extract the least significant
bit with AND operator and the resulting value is word parity.

Here is the code snippet that implements this technique to compute parity of 4, 8,
12, 16, 32 or 64 bit words:

{% highlight swift %}

// we use Int as suggested by Swift user manual
func parity_v4(x: Int) -> Int {
    var word = x
    // xor and shift (32-bit operand)
    word ^= word >> 32
    // xor and shift (16-bit operand)
    word ^= word >> 16
    // xor and shift (8-bit operand)
    word ^= word >> 8
    // xor and shift (4-bit operand)
    word ^= word >> 4
    // xor and shift (2-bit operand)
    word ^= word >> 2
    // xor and shift (1-bit operand)
    word ^= word >> 1
    // word parity is the least-significant bit (LSB)
    return word & 1
}

// 4-bit words
parity_v4(x: 0b0000) == 0 // true
parity_v4(x: 0b0001) == 1 // true
parity_v4(x: 0b0010) == 1 // true
parity_v4(x: 0b1000) == 1 // true

// 8-bit words
parity_v4(x: 0b0100_1000) == 0 // true

// 16-bit words
parity_v4(x: 0b0000_0001_0100_1000) == 1 // true
parity_v4(x: 0b0001_0001_0100_1000) == 0 // true

{% endhighlight %}

This version of the code features _O(log n)_ time complexity where _n_ is the word size,
saves us from the need to keep the cache of previously computed results, very succinct
and performant.