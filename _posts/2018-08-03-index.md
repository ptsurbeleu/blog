---
title: Compute X to the power of Y using math and bitwise operators
layout: post
---
### Problem statement
Write a function that takes a double __X__ and an integer __Y__ and computes __X__ to the power of __Y__. Keep in mind your code needs to meet the following constraints:

  - use `mathematical` (+, -, * and etc.) and `bitwise` operators;

```
  EXAMPLE:

  Input: x = 4.0, y = 2

  Output: 16.0
```

### Solution
Here is an actual code that implements a solution to raise __X__ to the power of __Y__ using __mathematical and bitwise__ operators:
```swift
func exp(x: Double, y: Int) -> Double {
    // Prepare state of the algorithm
    var total = 1.0, exponent = y, xx = x
    // Calculate X's exponent while there are bits to work with
    while exponent != 0 {
        // Append to the final result only when exponent's LSB is 1
        if exponent & 1 == 1 {
            // Append accumulated intermediate value
            total *= xx
        }
        // Exploit properties of exponentiation and use less
        // multiplications - eq. memoize and reuse intermediate results
        xx *= xx
        // Shift to the right by 1, since current bit
        // has been already processed
        exponent >>= 1
    }
    // Here is the final result
    return total
}
```
And below is the traditional section with assertions to validate the code performs as advertised and establish a baseline for any improvement work in the future:
```swift
// Assert a few simple cases (including the original one)
exp(x: 4.0, y: 2) == 16.0
exp(x: 2.0, y: 3) == 8.0
exp(x: 2.0, y: 2) == 4.0
exp(x: 2.0, y: 1) == 2.0
exp(x: 2.0, y: 0) == 1.0
```

### Bits & pieces
This solution applies `iterative squaring` to use less multiplications and `memoization` to reuse previous results.

Lets get started!

We begin with declaring a few variables that the code uses to store the `final result` of calculation, local copy of the `exponent` and `base`. Local copies of the `exponent` and `base` are needed since function parameters treated as constants by the compiler:
```swift
// Prepare state of the algorithm
var total = 1.0, exponent = y, xx = x
```

At this point, the code is ready to begin calculate `total` and perform computation while `exponent` does not equal to `0`, which means there is still some more work to do:
```swift
// Calculate X's exponent while there are bits to work with
while exponent != 0 {
  ... inner block of code ...
}
```

Inside the main loop, we match `least-significant bit` of the `exponent` to equal `1` and use it as a signal to append intermediate results to the `total` variable:
```swift
// Append to the final result only when exponent's LSB is 1
if exponent & 1 == 1 {
    // Append accumulated intermediate value
    total *= xx
}
```

Next statement exploits mathematical properties of exponentiation and uses less multiplications to calculate the answer, eq. on an every iteration it raises current value of `xx` to the power of `2` and that allows to reduce amount of multiplication operations required to calculate the final answer:
```swift
// Exploit properties of exponentiation and use less
// multiplications - eq. memoize and reuse intermediate results
xx *= xx
```

The following statement decreases the `exponent` by half, using `bitwise right shift` operator to match `xx`'s changes from the previous statement (eq. `exponent` decreases while `xx` increases):
```swift
// Shift to the right by 1, since current bit has been already processed
exponent >>= 1
```

Once the main loop has ended, `total` variable used inside the loop should have by now the final answer of the calculation performed:
```swift
// Here is the final result
return total
```

That is it!

Here is a Swift playground for this article at Github: [Compute X to the power of Y using math and bitwise operators](http://bit.ly/2O9Rj3z).

