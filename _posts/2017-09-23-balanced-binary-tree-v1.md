---
layout: post
title: Balanced binary tree (v1)
---

### Problem Statement
Implement a function to check if a binary tree is balanced. For the purposes
of this question, a balanced tree is defined to be a tree such that the heights
of the two subtrees of any node never differ by more than one.

### Recursive (2x)
Since we've been told what defines a balanced tree, we can traverse the tree
using recursion, calculate the height of each subtree and then make a comparison
whether they differ by no more than one.

{% highlight swift %}
// Tree node definition (recursive data structure).
class Node {
    // Data, left and right nodes
    var data: String = "", left: Node?, right: Node?
}

// Helper function to calculate height of any given tree.
func height(tree: Node?) -> Int {
    // base case
    if tree == nil { return 0 }
    // calculate left subtree
    let left = height(tree: tree!.left)
    // calculate right subtree
    let right = height(tree: tree!.right)
    // choose the larger one and add one,
    // since current level height is 1...
    return max(left, right) + 1
}

// Main function that solves the problem
func balanced(tree: Node?) -> Bool {
    // base case, eq. empty tree is always balanced
    if tree == nil { return true }
    // calculate left subtree
    let left = height(tree: tree?.left)
    // calculate right subtree
    let right = height(tree: tree?.right)
    // calculate absolute value of the difference between subtrees,
    // since we can't use negative value to make decision...
    let diff = abs(left - right)
    // tree IS NOT balanced, since difference is more than 1
    if diff > 1 { return false }
    // recurse into the left and right subtrees
    return balanced(tree: tree?.left) && balanced(tree: tree?.right)
}

// validations
// build a 2x2 tree (balanced)
let twoByTwo = withHeight(n: 2, m: 2)
// assert the tree is balanced, as expected
balanced(tree: twoByTwo) == true // true

// build a 3x2 tree (still balanced)
let threeByTwo = withHeight(n: 3, m: 2)
// assert the tree still balanced, as expected
balanced(tree: threeByTwo) == true // true

// build 5x3 tree (not balanced)
let fiveByThree = withHeight(n: 5, m: 3)
// assert the tree is not balanced, as expected
balanced(tree: fiveByThree) == false // true
{% endhighlight %}

This approach works beatifully and quite easy to digest. Though it is not
very efficient. Why would it be so? Notice, __balanced__ function calls
recursively itself again and again, to calculate heights of left and right
subtrees on __each iteration__. Therefore, this code snippet's time complexity
is __O(N^2)__.

Hence the name of the technique, __recursive (2x)__.

__P.S.__ Here are the helper functions used above to generate binary trees
of specified height for validation purposes.

{% highlight swift %}
// Helper function to build a balanced tree of the given height
func withHeight(h: Int) -> Node? {
    // base case, nothing can be build
    if h == 0 { return nil }
    // build new root node
    let node = Node()
    // build left subtree
    node.left = withHeight(h: h - 1)
    // build right subtree
    node.right = withHeight(h: h - 1)
    // here is our tree
    return node
}

// Helper function to build a tree of the given heights
func withHeight(n: Int, m: Int) -> Node? {
    // our root node
    let node = Node()
    // build left subtree of height N
    node.left = withHeight(h: n)
    // build right subtree of height M
    node.right = withHeight(h: m)
    // here is our tree
    return node
}
{% endhighlight %}