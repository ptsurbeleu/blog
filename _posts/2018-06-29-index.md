---
title: Number of bits required convert x to y
tags: [bitwise, binary]
---
### Problem statement
Given two integers `x` and `y`, write a function to find out a number of bits required to convert one integer to the other.
```
  EXAMPLE

    Input: x = 0b1000; y = 0b0011;

    Output: 3
```

### Solution
Here is the actual code that implements a solution to find how many bits required to convert `x` to `y`:
```swift
func bitsToConvert(x: UInt, y: UInt) -> Int {
    // Prepare state
    var bits = 0, diff = x ^ y
    // Loop thru diff until it runs out of bits
    while diff > 0 {
        // Plus one more bit that is different
        bits += 1
        // Flip the next least-significant bit
        diff &= diff - 1
    }
    // This is how many bits are required (eq. different)
    // to convert x to y
    return bits
}
```

And here is our traditional section with assertions to validate the code performs as advertised and establish a baseline for any improvement work in the future:
```swift
// Assert a few simple cases
bitsToConvert(x: 0b0000, y: 0b0000) == 0
bitsToConvert(x: 0b0001, y: 0b0000) == 1
bitsToConvert(x: 0b0011, y: 0b0001) == 1
bitsToConvert(x: 0b0010, y: 0b0001) == 2

// Assert original case
bitsToConvert(x: 0b1000, y: 0b0011) == 3

// Assert a few edge cases using hexadecimal notation
bitsToConvert(x: 0xffff_ffff_ffff_ffff, y: 0xffff_ffff_ffff_ffff) == 0
bitsToConvert(x: 0xffff_ffff_ffff_ffff, y: 0xffff_ffff_ffff_fffe) == 1
bitsToConvert(x: 0xffff_ffff_ffff_ffff, y: 0xffff_ffff_ffff_fff0) == 4
bitsToConvert(x: 0xffff_ffff_ffff_ffff, y: 0x0000_0000_0000_0000) == 64
bitsToConvert(x: 0xffff_ffff_ffff_ffff, y: 0x0000_0000_0000_0001) == 63
```

### Bits & pieces
This problem statement may feel a bit complicated but in fact it is not and walking thru the solution details step-by-step should help you to master understanding of the key components in this case, such as use of an `exclusive or (XOR)` operator and `flip least-significant bit` technique.

Lets get started!

This first line is about preparing state of the algorithm, where `bits` variable would contain the answer by the end, eq. the number of bits required to convert `x` into `y` and `diff` variable contains all the bits that are different between `x` and `y` (eq. required to convert `x` into `y`):
```swift
// Prepare state
var bits = 0, diff = x ^ y
```

Next is a loop that iterates over bits in `diff` variable that are different and since we use `flip least-significant bit` technique to count bits, the exit criteria from the loop is when `diff` has value of `0` indicating there are no more bits left to evaluate:
```swift
// Loop thru diff until it runs out of bits
while diff > 0 {
  ...
}
```

The code inside the loop goes through each & every bit and here we increment `bits` counter by `1` on an every iteration to count in yet another bit and by the end `bits` variable would have our final answer:
```swift
// Plus one more bit that is different
bits += 1
```

Here is the `flip least-significant bit` technique at work helping us out to count remaining bits in `diff` variable:
```swift
// Flip the next least-significant bit
diff &= diff - 1
```

Once we are done flipping all the bits that were different the loop would have exited by now and `bits` variable should have the final answer readily available for us to return to the caller:
```swift
// This is how many bits are required (eq. different) to convert x to y
return bits
```

And we are done! 💎

Here is a Swift playground for this article at Github: [Number of bits required to convert X to Y](http://bit.ly/2Myxn9X).

Hope you will find the article useful!