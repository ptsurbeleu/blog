---
layout: post
title: Quick links with bit.ly from terminal
---

Some time ago working on yet another documentation snippet sprinkled with links,
I caught myself copying/pasting and shortening links in the browser using
[bit.ly](http://bit.ly). It is certainly an option to shorten
a link in the browser... but I was curious if there is a way to shorten hyperlink,
say in the terminal?

Search on the subject yielded no relevant results and I decided to give it a try
to put together a shell function to accomplish just what needed.

Starting point was [Bitly API Documentation](http://dev.bitly.com)
and there a gem was found in [Links section (v3)](http://dev.bitly.com/links.html#v3_shorten).

From the first glance Bitly API description looks just like [gitignore.io's](http://bit.ly/1UlAccb)
API... Mmm, why not to try out wrap a call to Bitly's __v3/shorten__ endpoint in
a shell function using [curl](http://bit.ly/1S5eUlH)? Here we go...

{% highlight bash %}

# bit.ly
function bitly() {
    curl -s \
         --data-raw "access_token=••••" \
         --data-raw "format=txt" \
         --data-urlencode "longUrl=$@" \
         https://api-ssl.bitly.com/v3/shorten
}

{% endhighlight %}

Looks neat and there is one more thing (__access_token__) to insert before it is usable.

There is [Manage OAuth Apps](https://bitly.com/a/oauth_apps)
section (you need to be a registered user to be able to access this section) where
you can get a General Access Token to set access_token parameter in the snippet above
when call to Bitly API.

Let's pretend the value for the token was __1ef0d67ca__.

You may already be familiar with [~/.bashrc_profile](http://bit.ly/1JIX9VI)
but in case if you aren't it serves as a permanent storage for your account's profile,
where you can keep useful aliases and functions.

Here's how it looks in my ~/.bashrc_profile:

{% highlight bash %}

# bit.ly - with a fake token defined above
function bitly() {
    curl -s \
    --data-raw "access_token=1ef0d67ca" \
    --data-raw "format=txt" \
    --data-urlencode "longUrl=$@" \
    https://api-ssl.bitly.com/v3/shorten
}

{% endhighlight %}

Once you're done saving the snippet in ~/.bashrc_profile, remember to launch
a new terminal window (or tab) so these changes to the profile will take in effect.

Now the new __bitly__ helper is available in the terminal and it neatly enhances  
task of shortening a link:

{% highlight bash %}

# shorten http://google.com link
~# bitly http://google.com
http://bit.ly/1KBpB7t

# shorten http://google.com link and copy the result to the clipboard
~# bitly http://google.com | pbcopy

{% endhighlight %}

Hope you will find these hints useful.
