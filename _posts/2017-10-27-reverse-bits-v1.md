---
layout: post
title: Reverse bits (v1)
---
### Problem Statement
We need to write a function that takes a 64-bit word and returns 64-bit word
consisting of the bits of the input word in reverse order. For example, if the
input is alternating 1s and 0s, i.e., (_1010...10_), the output should be
alternating 0s and 1s, i.e., (_0101..01_).

_Hint: Use a lookup table._

### Brainstorming
In case we will call the function once in a while, a very simple brute-force
approach would work magic. What binary operator(s) can help us to accomplish that?
It seems with only `>> (RIGHT SHIFT)`, `<< (LEFT SHIFT)` and `^ (XOR)` operators
we should be able to perform the computation.

```swift
// XOR truth table
0 ^ 0 == 0 // true
1 ^ 1 == 0 // true
0 ^ 1 == 1 // true
1 ^ 0 == 1 // true
```

Lets work on reversing bits manually using 6-bit word for the sake of simplicity,
such as `0b101_000`:

```swift
// manually reverse bits for 0b101_000 ⇢ 0b000_101

// since XOR can flip the bits for us, we need a bitmask that has 1s
// only in the following two positions: MSB and LSB...

// using that technique we logically "divide" our 6-bit mask
// in two parts, such as

// • MSB - bits 4, 5 and 6
// • LSB - bits 1, 2 and 3

// MSB | LSB
// 100 | 001

// with 6-bit word, that means there are 3 {MSB, LSB} pairs:
// • 1st pair - {5, 0} ⇢ 0b100_001
// • 2nd pair - {4, 1} ⇢ 0b010_010
// • 3rd pair - {3, 2} ⇢ 0b001_100

// here is our 1st pair bitmask {5, 0} ⇢ 100_001
// x ^ (1 << 5 | 1 << 0), where 5 - MSB and 0 - LSB
0b101_000 ^ 0b100_001 == 0b001_001 // true

// move on to the next pair of MSB and LSB - {4, 1}
// here is our 2nd pair bitmask {4, 1} ⇢ 010_010
// however, we don't swap that pair since MSB(4) == LSB(1)
(0b001_001 >> 4) & 1 == (0b001_001 >> 1) & 1 // true

// move on to the last pair of MSB and LSB - {3, 2}
// here is our 3rd pair bitmask {3, 2} ⇢ 001_100
// x ^ (1 << 3 | 1 << 2)
0b001_001 ^ 0b001_100 == 0b000_101 // true, that is our answer
```

With that being said, we can build an actual implementation using
the technique we just explored (_supports 8-bit words_):

```swift
func reverseBits(input: Int) -> Int {
    // prepare required state, with MSB = 7 for 8-bit word support
    // ... or MSB = 63 for 64-bit word support
    var output = input, LSB = 0, MSB = 7
    // brute-force way to compute the reverse of the input
    while MSB > LSB {
        // swap bits only when they differ
        if (output >> MSB) & 1 != (output >> LSB) & 1 {
            // in-place bits swap operation
            output ^= (1 << MSB | 1 << LSB)
        }
        // LSB bitmask moves to the left by one
        LSB += 1;
        // MSB bitmask moves to the right by one
        MSB -= 1;
    }
    // send computation results back to the caller
    return output
}

// validations for simple cases
reverseBits(input: 0b0000_0001) == 0b1000_0000
reverseBits(input: 0b0000_0011) == 0b1100_0000
reverseBits(input: 0b0000_0010) == 0b0100_0000
reverseBits(input: 0b0000_0100) == 0b0010_0000
reverseBits(input: 0b0000_1000) == 0b0001_0000
// validations for slightly involved cases
reverseBits(input: 0b1000_1000) == 0b0001_0001
reverseBits(input: 0b1000_0001) == 0b1000_0001
// validation for even more involved case
reverseBits(input: 0b1001_0011) == 0b1100_1001
```

This brute-force approach works without any doubt but it has one flaw,
we didn't use the hint provided.

In the next article, we will explore how to leverage the hint and compute
the result faster and smarter.