---
layout: post
title: Computing the parity of a word (v3)
---

In the [previous (slightly faster) version]({% link _posts/2017-08-25-computing-parity-of-a-word-v2.md %}) of the code snippet we addressed performance concerns but our curiosity begged for more,
lets see if there is a way to something that is even better.

### Compute parity (large number of words)
The other constraint is a very large number of words and there are two key techniques
to performance, processing multiple bits at a time and caching results in an array-based
lookup table.

First, lets take a look how the caching technique works. Here is an example code snippet
outlining an array-based parity lookup table for 4-bit words that nicely fits in memory,
since it requires only 16 _(2 ^ 4)_ array entries:

{% highlight swift %}

// array-based lookup table definition, where index is a lookup key
var cache = [UInt]()
// manually compute parity for all 4-bits words
cache[0b0000] = 0 // where 0b0000 = 0
cache[0b0001] = 1 // where 0b0001 = 1
...
cache[0b0111] = 1 // where 0b0111 = 7
cache[0b1000] = 1 // where 0b1000 = 8
...
cache[0b1110] = 1 // where 0b1110 = 14
cache[0b1111] = 0 // where 0b1111 = 15

{% endhighlight %}

Using the lookup table above, we can quickly find out parity answer for any given 4-bit
word. This is how we would use it in our code:

{% highlight swift %}
// lets say we have the following 4-bit words
let _1st = 0b1000, _2nd = 0b0111, _3rd = 0b1111

// notice how bit patterns of these words correspond
// to the indices in the lookup table above.

cache[_1st] == 1 // 8th item in the table
cache[_2nd] == 1 // 7th item in the table
cache[_3rd] == 0 // 15th item in the table

{% endhighlight %}

This is the actual code snippet using caching technique described above _(supports 16-bit words)_:

{% highlight swift %}

// we use Int as suggested by Swift user manual
func parity_v3(x: Int) -> Int {
    // precomputed parity cache of 4-bit words
    let cache: [Int] = [0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0]
    // our 4-bit word mask in binary, or 0xf in hex
    let mask: Int = 0b1111
    // our 4-bit blocks and remember that
    // array indices are signed integers
    let _1st = (x >>  0) & mask
    let _2nd = (x >>  4) & mask
    let _3rd = (x >>  8) & mask
    let _4th = (x >> 12) & mask
    // compute parity by XORing all blocks,
    // since 0 ^ 1 = 1, 0 ^ 0 = 0 and 1 ^ 1 = 0
    return cache[_1st] ^ cache[_2nd] ^ cache[_3rd] ^ cache[_4th]
}

{% endhighlight %}

Here I put together a few validations for different word sizes:

{% highlight swift %}

// 4-bit words
parity_v3(x: 0b0000) == 0 // true
parity_v3(x: 0b0001) == 1 // true
parity_v3(x: 0b0010) == 1 // true
parity_v3(x: 0b1110) == 1 // true

// 8-bit words
parity_v3(x: 0b0001_1110) == 0 // true
parity_v3(x: 0b1001_1110) == 1 // true

// 12-bit words
parity_v3(x: 0b0001_1001_1110) == 0 // true
parity_v3(x: 0b0101_1001_1110) == 1 // true
parity_v3(x: 0b1101_1001_1110) == 0 // true

// 16-bit words
parity_v3(x: 0b0010_1101_1001_1110) == 1 // true
parity_v3(x: 0b0011_1101_1001_1110) == 0 // true
parity_v3(x: 0b1011_1101_1001_1110) == 1 // true

{% endhighlight %}

This version addresses all of the remaining concerns and also uses the hint provided.
We also improved time complexity, which is _O(n/L)_ now, where _n_ is the word size
_(4, 8, 12 or 16 bit in the code above)_ and _L_ is the size _(4 bit in the code above)_
of the words we cache results for.

But... there is one more cool thing we can do using insights from v3 version of the
code snippet.

Lets see what it is in the next post.