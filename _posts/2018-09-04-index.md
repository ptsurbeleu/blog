---
title: The Dutch national flag sorting
---
### Problem statement
Write a function that takes an array `n` and an index `i`, and sorts the elements so that all elements less than `n[i]` (the `sortBy`) appears first, followed by elements equal to `sortBy`, followed by the elements greater than `sortBy`.

```
EXAMPLE:

  Input: n = [1,2,3,1,2,3]; i = 4

  Output: [1,1,2,2,3,3]
```

### Solution
Here is the actual code that implements that type of sorting technique, (*aka. __the Dutch national flag sorting__*):
```swift
func sort(n: [Int], i: Int) -> [Int] {
    // Query element's value to sort by
    let sortBy = n[i]
    // Prepare state of the algorithm
    var nn = n, less = 0, same = 0, more = n.count
    // Keep sorting while there are unsorted elements
    while same < more {
        if nn[same] < sortBy {
            // Swap elements that are less and same
            swap(&nn, less, same)
            // Move on to the next pair of elements
            less += 1; same += 1
        } else if nn[same] == sortBy {
            // Move on to the next same element
            same += 1;
        } else {
            // Move on the the previous element
            more -= 1
            // Swap elements that are same and more
            swap(&nn, same, more)
        }
    }
    // Here is our answer
    return nn
}
```

Following snippet is the traditional section with assertions to validate the code performs as advertised and establish a baseline for any improvement work in the future:
```swift
// Assert a few test cases
sort(n: [1,2,3,1,2,3],    i: 4) == [1,1,2,2,3,3]
sort(n: [0,1,0,1,0,1],    i: 2) == [0,0,0,1,1,1]
sort(n: [-1,0,-1,0,-1,0], i: 1) == [-1,-1,-1,0,0,0]
```

### Bits & pieces
This coding challenge might be somewhat vague to throughly understand from the first attempt but after a few tries you should be able to understand the way its inner parts work and apply these to the similar coding challenges.

Lets get started!

The first statement makes a copy of the element's value that the caller has requested to sort by, since later array items will be reordered making a copy of the value is a must:
```swift
// Query element's value to sort by
let sortBy = n[i]
```

Moving on to the next statement, this is the code to prepare a few variables that maintain state of the sorting algorithm's internal parts.

Such as, `nn` is a local copy of the array with items to be sorted, while variables `less` and `same` are indexes that refer to the first element of sequences with values that are either __less__ or __same__ as the `sortBy` value.

The remaining `more` variable refers to the index of the last element in the sequence with values that are greater than `sortBy` value:
```swift
// Prepare state of the algorithm
var nn = n, less = 0, same = 0, more = n.count
```

This statement, is the __main loop__ that the algorithm uses to iterate and rearrange elements in the given array into 3 different groups, items that are __less__, __same__ or __greater__ than the value requested to sort by.

Since index of the `same` sequence is the one that moves forward, while index of the `more` sequence the only one that moves backwards - the loop uses this as a to signal that all elements in the array are rearranged in the desired order:
```swift
// Keep sorting while there are unsorted elements
while same < more {
  ... inner block of code ...
}
```

Within the loop, there are __3__ code snippets that perform sorting manipulations and rearrange elements of the array.

The first snippet checks whether the current element of the `same` sequence is smaller than `sortBy`'s value and if that's the case it performs an in-place swap between current element of `less` and `same` sequences and advances sequence indexes to the next position, since these two elements are now in the desired order:
```swift
if nn[same] < sortBy {
  // Swap elements that are less and same
  swap(&nn, less, same)
  // Move on to the next pair of elements
  less += 1; same += 1
} ...
```

This second snippet checks whether the current element of the `same` sequence equals to `sortBy` value and in that case it simply advances to the index of the next element in the sequence (_no swap is needed, since element already has the desired order_):
```swift
  ...
} else if nn[same] == sortBy {
  // Move on to the next same element
  same += 1;
} ...
```

The third and the last snippet implicitly handles the case when the current element of the `same` sequence is greater than `sortBy`'s value and advances index to the previous element in the `more` sequence followed by an in-place swap between elements of these sequences to put `more`'s elements into the desired order:
```swift
  ...
} else {
    // Move on the the previous element
    more -= 1
    // Swap elements that are same and more
    swap(&nn, same, more)
}
```

Finally, as soon as the __main loop__ is completed therfore all three sequence are now rearranged in the desired order (_eq. first group of values that are smaller, second group of values that are the same and the third group of values that are greater_).

Since it's all done and the code simply returns `nn` collection to the caller:
```swift
// Here is our answer
return nn
```

That's all folks! 👔

Here is a Swift playground for this article at Github: [The Dutch national flag sorting)](http://bit.ly/2PEmrZP).