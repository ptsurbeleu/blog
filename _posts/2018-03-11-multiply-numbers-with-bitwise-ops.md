---
layout: post
title: Multiply numbers using bitwise operators
---

### Problem Statement
Write a program that multiplies two integers `a` and `b`. Keep in mind your code needs to meet the following constraints:
  - use only bitwise operators `<<`, `>>`, `|`, `&`, `~` and `^`;
  - use assignment operator `=`;
  - use equality checks and `boolean` combinations;

### Solution
Here is the actual code that implements multiplication of two numbers without use of any arithmetic operators and meets the requirements:

```swift
// Multiplies two numbers using bitwise operators only
func multiply(x: Int, y: Int) -> Int {
    // Since function parameters in Swift are constants,
    // we declare internal variables that we can modify
    var xx = x, yy = y, sum = 0
    // Iterate on xx while there are bits to work with
    while xx != 0 {
        // Compute sum only when LSB of xx is 1
        if xx & 1 == 1 {
            sum = add(x: sum, y: yy)
        }
        // Shift xx by 1 to the right (negative & positive numbers)
        xx = xx >>> 1
        // Shift yy by 1 to the right (eq. yy * 2)
        yy <<= 1
    }
    // Here is the result of multiplication
    return sum
}
```

Here goes our traditional block of assertions to confirm the code performs as advertised and establish a baseline for any improvement work in the future:

```swift
// assert some basic cases (positive range)
multiply(x: 0, y: 0) == 0b0000 // (0)
multiply(x: 1, y: 0) == 0b0000 // (0)
multiply(x: 0, y: 1) == 0b0000 // (0)
multiply(x: 3, y: 3) == 0b1001 // (9)
multiply(x: 3, y: 4) == 0b1100 // (12)

// assert some basic cases (negative range)
multiply(x: -1, y: -1) == 0b0001 // (1)
multiply(x: -1, y:  0) == 0b0000 // (0)
multiply(x:  0, y: -1) == 0b0000 // (0)
multiply(x: -3, y: -1) == 0b0011 // (3)
multiply(x: -3, y: -3) == 0b1001 // (9)

// assert some basic cases (negative + positive range)
multiply(x: -1, y: 2) == µ(0xffff_ffff_ffff_fffe) // (-2)
multiply(x: -2, y: 8) == µ(0xffff_ffff_ffff_fff0) // (-16)
multiply(x: -3, y: 3) == µ(0xffff_ffff_ffff_fff7) // (-9)
multiply(x: -3, y: 4) == µ(0xffff_ffff_ffff_fff4) // (-12)
```

### Bits & pieces
As you will see this code snippet is very straightfoward and easy to understand. Each line in the final solution carries a simple task and all the lines collectively build an impressive solution that multiplies two numbers without using arithmetic operators at all.

Lets get started!

```swift
// Since function parameters in Swift are constants,
// we declare internal variables that we can modify
var xx = x, yy = y, sum = 0
```

Here we are getting ready the state to be used later in the computation. Did you notice that function parameters `x` and `y` are re-assigned to the local variables `xx` and `yy`?

This is needed this because function parameters are constants and Swift compiler won't allow to change value of `x` and `y` inside the function:

> “Function parameters are constants by default. Trying to change the value of a function parameter from within the body of that function results in a compile-time error. This means that you can’t change the value of a parameter by mistake.”
> 
> Excerpt From: [Apple Inc. “The Swift Programming Language (Swift 4.0.3).” iBooks](https://apple.co/2CIRCMy).

```swift
// Iterate on xx while there are bits to work with
while xx != 0 {
    ... inner block of code ...
}
```
Here we are going to iterate over `xx` variable until its value becomes `0`, which appears to be a signal that there are no more bits left to work with and we are done calculating the result of multiplication.

```swift
// Compute sum only when LSB of xx is 1
if xx & 1 == 1 {
    sum = add(x: sum, y: yy)
}
```
This code adds values of `yy` and `sum` together and puts the result back into `sum` variable to keep track of the intermediate results. Keep in mind, this only happens when `least-significat bit (aka. LSB)` of `xx` is set to `1`!

```swift
// Shift xx by 1 to the right (negative & positive numbers)
xx = xx >>> 1
```
At this point we perform a logical bitwise right shift on `xx`. It is called `logical` since it doesn't fill bits on the left with the value of the sign bit, where it is `1` for negative and `0` for positive numbers.

Since Swift doesn't have support for `logical` bitwise right shift operator, we can do it ourselves and this is the code snippet to get that working:
```swift
// Logical bitwise right shift operator declaration
infix operator >>>: BitwiseShiftPrecedence

// Logical bitwise right shift operator implementation
func >>>(x: Int, y: Int) -> Int {
  // Convert both signed and unsigned numbers and shift bits
  let bits = UInt(bitPattern: x) >> UInt(bitPattern: y)
  // Translate resulting bits into a signed integer
  return Int(bitPattern: bits)
}
```

On an every iteration `logical bitwise right shift` instruction shifts `xx` by one bit to the right, effectively halving `xx` and getting it closer to the value of `0`, that is a signal our computation is done.

```swift
// Shift yy by 1 to the right (eq. yy * 2)
yy <<= 1
```
This instruction has an effect of actually doubling `yy` for the every shift we make to the left. The reason for doing that is to be able to calculate a valid answer.

Believe it or not, we are building a bitwise multiplication table here and this is how the table would look like as if we were to use pen & paper to write down steps to multiply numbers:
```swift
 1 * 2 == 2 << 0 // 2
 2 * 2 == 2 << 1 // 4
 3 * 2 == 1  * 2 + 2  * 2
       == 2 << 0 + 2 << 1 // 2 + 4 = 6
 4 * 2 == 2 << 2 // 8
```
It is essentially our foundation to build upon and using this technique we can easily multiply any two numbers, for example `5 * 2`:
```swift
 5 * 2 == 1  * 2 + 4  * 2
       == 2 << 0 + 2 << 2 // 2 + 8 = 10
```
As you can see, this is a solid framework to perform multiplication of any two given positive and negative numbers.

```swift
// Here is the result of multiplication
return sum
```
And the last line simply returns the result of multiplication to the calling code.

### Peformance
To understand time and space complexity of the solution, we need to remember that this code snippet must touch all bits of the given number that are set to `1`. With that in mind, there is a case when there are all (64) bits are set to `1` and that number is `-1` - it would take us 64 iterations to finish the computation. So that brings is into `O(n)` league in terms of __time complexity__.

The code we have above, actually very conservative when it comes to the space required to store indermediate results of the computation, there is only one variable `sum` and that is all it takes. That brings us into `O(1)` league in terms of __space complexity__.

### Summary
Bitwise operators and raw bits manipulations do take some time to get comfortable with, however once you nailed it - every next challenge you encounter will take less time and energy to solve.

Hope you will find the article useful!