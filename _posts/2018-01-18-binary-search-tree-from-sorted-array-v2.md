---
layout: post
title: Binary search tree with minimal height from sorted array (v2)
---
Our previous version of the code snippet did solve the problem at hand and as we discovered its time complexity was __O(N log N)__ but other than that it solves the problem at hand. In this post we will explore a way to improve the time complexity and make it a bit faster.

### Brainstorming
Just to recap, time complexity of __O(N log N)__ was because to insert a new value, first we must find a valid spot for the value to be inserted at and that is the __factor__ that incurs extra __log N__ cycles:

```swift
// inserts a new node with the specified value
public func insert(n: Int) {
    ...
    // find the insertion point, eq. parent node)
    let parent = root.findParent(n: n) // O(log N)
    ...
}
```

Although, if we can come up with a way to build the tree without using find operation we could eliminate `log N` factor and get our solution into __O(N)__ time complexity category.

How we would do that? It turns out, there is already code snippet in the [previous version]({% post_url 2018-01-04-binary-search-tree-from-sorted-array-v1 %}#hands-on-experience) that works that way and here it is:

```swift
// our input
let input = [1, 2, 3]
// define the operational boundaries using arithmetic mean technique
var start = 0, end = input.count, middle = (start + end) / 2

// init root of the tree, with the value in the middle of the array
let root = Node(value: input[middle])

// compute the middle of the sub-array on the left ([0...1])
var left = (start + middle) / 2
// init left subtree with the value in the middle of the sub-array
root.left = Node(value: input[left])
// are there more items left to process on the left? nope...
left != 0 // false

// compute the middle of the sub-array on the right ([1...2])
var right = (middle + end) / 2
// init right subtree with the value in the middle of the sub-array
root.right = Node(value: input[right])
// are there more items to process on the right? nope...
right + 1 < end // false
// we are done building our binary tree

```

The snippet above builds binary search tree from a sorted array and its time complexity is __O(N)__, since we don't perform find operation for an every value to be inserted. We simply wrap each value into a tree node and connect nodes in the scope with each other.

Our next thing would be to wrap it somehow, so that it can be used in the final solution and fortunately with recusion it is not that complicated:

```swift
// Converts sorted array into a set of nodes using recursion
func convert(arr: [Int], x: Int, y: Int) -> Node? {
    // are there more items left to process?
    if x > y {
        return nil
    }
    // compute the middle of the sub-array
    let middle = (x + y) / 2
    // init root of the given sub-tree
    let node = Node(value: arr[middle])
    // init left sub-tree
    node.left = convert(arr: arr, x: x, y: middle - 1)
    // init right sub-tree
    node.right = convert(arr: arr, x: middle + 1, y: y)
    // we are done building our sub-tree
    return node
}
```

What you see above would be exactly the snippet that builds all the nodes for our binary search tree from a sorted array featuring time complexity of __O(N)__. Now the only little caveat left is to fit this code into `BinarySearchTree` class and validate the implementation using [existing tests]({% post_url 2018-01-04-binary-search-tree-from-sorted-array-v1 %}).

### Solution
Here is our final solution that uses the code snippet we come up with as is:

```swift
// BinarySearchTree extension with the scenario-specific operations
extension BinarySearchTree {
    // Inserts the specified value into the tree.
    public func fromSortedArray(arr: [Int]) {
        // recursively convert our input into binary search tree
        self.root = convert(arr: arr, x: 0, y: arr.count - 1)
    }

    // Recursively converts sorted array into a binary search tree
    func convert(arr: [Int], x: Int, y: Int) -> Node? {
        // our base case
        if x > y {
            return nil
        }
        // find the middle of the array
        let middle = (x + y) / 2
        // instantiate a new parent node
        let node = Node(value: arr[middle])
        // instantiate left subtree (if any)
        node.left = convert(arr: arr, x: x, y: middle - 1)
        // instantiate right subtree
        node.right = convert(arr: arr, x: middle + 1, y: y)
        // yield results of our effort back to the calling code
        return node
    }
}
```

Following is traditional section with assertion snippets that validate correctnes of the final solution:

```swift
// prepare state
let treeOfSix = BinarySearchTree()

// convert array into binary search tree
treeOfSix.fromSortedArray(arr: [1, 2, 3, 4, 5, 6])

// validate the tree using monogram notation
assert(expected: 1, monogram: "1L", startFrom: treeOfSix.root!) // true
assert(expected: 2, monogram: "2R", startFrom: treeOfSix.root!) // true
assert(expected: 3, monogram: "0T", startFrom: treeOfSix.root!) // true
assert(expected: 4, monogram: "2L", startFrom: treeOfSix.root!) // true
assert(expected: 5, monogram: "1R", startFrom: treeOfSix.root!) // true
assert(expected: 6, monogram: "2R", startFrom: treeOfSix.root!) // true
```

Afterall, it was quite straightforward to improve performance of the existing solution as long as we have clearly attributed each individual part of the time complexity profile to their respective components.

Hope you will find the article useful!