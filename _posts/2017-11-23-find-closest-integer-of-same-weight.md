---
layout: post
title: Find a closest integer with the same weight
---
### Problem Statement
Define the _weight_ of a nonnegative integer _x_ to be the number of bits that are set to 1 in its binary representation. For example, since 92 in base-2 equals to `1011100`, the weight of 92 is 4.

Write a program which takes as input a nonnegative integer _x_ and returns a number y which is not equal to _x_, but has the same weight as x and their difference, `|x - y|`, is as small as possible. You can assume x is not 0, or all 1s. For example, if x = 6, you should return 5.

_Hint: Start with the least significant bit._

### Brainstorming
Lets try to take our example as a baseline for brainstorming. So, when the input is `6 (0b110)` our answer is `5 (0b101)`, since both numbers have the same weight of `2`. One fairly straightforward way to aprroach the problem would be to use arithmetic operator `-` to get from `6` to `5` and then compare the number of bits on both sides. However, in case when the answer is greater than the original number, we would use `+` operator to get to the number with the same weight.

Should be easy enough to write the code implementing this approach.

We would need one extra thing and that is `weight()` helper function to count numbers of bits set to `1` in any given number:
```swift
// returns number of bits set to 1 in the given number
func weight(_ x: Int) -> Int {
    // prepare for the work
    var n = 0, y = x
    // at 0 we should be done with the computation
    while y > 0 {
        // use "flip least significant byte" technique
        // to get O(log n) performance
        y = y & (y - 1)
        // simply increment bits count
        n += 1
    }
    // this is how many bits are set to 1 (eq. weight)
    return n
}
```

### Solution
As we found out above, using simple arithmetic operators would be a simple and straightforward way to solve the problem at hand.

Lets first try to compute the answer manually outlining the technique step-by-step using individual instructions and our helper function `weight()`.

```swift
// first example of 13 (0b1101), its weight = 3...
// 13 - 1 = 12 (0b1100) is not our answer, its weight = 2
weight(0b1101 - 1) == 3 // false
// 13 + 1 = 14 (0b1110) is our answer, its weight = 3
weight(0b1101 + 1) == 3 // true

// second example of 24 (0b11000), its weight = 2...
// 24 - 1 = 23 (0b10111) is not the answer, its weight = 4
weight(0b11000 - 1) == 2 // false
// 24 + 1 = 25 (0b11001) is not our answer, its weight = 3
weight(0b11001 + 1) == 2 // false
// 23 - 1 = 22 (0b10110) is not our answer, its weight = 3
weight(0b10111 - 1) == 2 // false
// 25 + 1 = 26 (0b11010) is not our answer, its weight = 3
weight(0b11001 + 1) == 2 // false
// 22 - 1 = 21 (0b10101) is not our answer, its weight = 3
weight(0b10110 - 1) == 2 // false
// 26 + 1 = 27 (0b11011) is not our answer, its weight = 4
weight(0b11010 + 1) == 2 // false
// 21 - 1 = 20 (0b10100) is our answer, its weight = 2
weight(0b10101 - 1) == 2 // true
```

As you can see, it is a simple technique to be implemented.

Here is the actual implementation of `find_closest()` function using the technique explored above:

```swift
func find_closest(x: Int) -> Int {
    // how far we are from the x on the left & right
    // numbers on the left less than x and on the right greater than x
    var y = 0, left = x - 1, right = x + 1, x_weight = weight(x) 
    // simply loop until the number is found
    while y == 0 {
        // count bits on the left
        if x_weight == weight(left) {
            y = left
            continue
        }
        // count bits on the right
        if x_weight == weight(right) {
            y = right
            continue
        }
        // move to the next pair of numbers
        left -= 1; right += 1
    }
    // here is our closest number with the same weight
    return y
}
```

And here are a few validations to ensure the code we came up with actually solving the problem as expected, using smaller numbers in `binary` notation (_for readability_):
```swift
// the closest to 0b00110 (6) is 0b00101 (5)
find_closest(x: 0b00110) == 0b00101 // true

// the closest to 0b01101 (13) is 0b01110 (14)
find_closest(x: 0b01101) == 0b01110 // true

// the closest to 0b11000 (24) is 0b10100 (20)
find_closest(x: 0b11000) == 0b10100 // true

// the closest to 0b10000 (16) is 0b01000 (8)
find_closest(x: 0b10000) == 0b01000 // true
```

However, there is a problem with this implementation and below is an experiment describing it, using bigger numbers in `hexadecimal` notation (_for readability_):
```swift
// takes up to 8,204 iterations to compute this answer
// the closest to 0x2000 (8192) is 0x1000 (4096)
find_closest(x: 0x2000) == 0x1000 // true

// takes up to 16,396 iterations to compute this answer
// the closest to 0x4000 (16384) is 0x2000 (8192)
find_closest(x: 0x4000) == 0x2000 // true

// takes up to 32,780 iterations to compute this answer
// the closest to 0x8000 (32768) is 0x4000 (16384)
find_closest(x: 0x8000) == 0x4000
```
As you might have noticed number of iterations to compute the answer gets exponentially worse and we are pretty much going to be stuck forever finding the answer as the numbers grow bigger and bigger.

This approach was very easy to implement but exhibits poor performance on certain inputs and also it doesn't use the hint provided.

So lets see in the next version how we could address these two issues at the same time using an interesting technique.