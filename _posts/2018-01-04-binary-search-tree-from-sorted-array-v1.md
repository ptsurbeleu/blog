---
layout: post
title: Binary search tree with minimal height from sorted array (v1)
---
### Problem Statement
Given a sorted (increasing order) array, write an algorithm to create a binary search tree with minimal height.

### Brainstorming
Definition of binary search tree states that the data structure keeps its keys in sorted order, so that any operation on the data structure can use the principle of binary search. This is very convenient, since it perfectly matches our input, which is _a sorted array_ and we can apply binary search principle to build a binary search tree with the minimal height from our input.

The search target on each iteration is the root of subtree and on each iteration we will be focusing only on that. Lets give it a try and discover how that would work for a few very simple cases.

Our first case to review is the input of `[1, 2, 3]`:

```
• here is the binary search tree for [1, 2, 3]
│
│   ┌── 3
└── 2
    └── 1
```

And our second case to review is the input of `[1, 2, 3, 4, 5, 6]`:

```
• here is the binary search tree for [1, 2, 3, 4, 5, 6]
│
│       ┌── 6
│   ┌── 5
│   │   └── 4
└── 3
    │   ┌── 2
    └── 1
```

As you can see each element becomes either root or leaf node and the only question left, is what technique we can use to identify each element's type and its neighbours (_if any_)? Good news, we can apply a well-known mathematical technique of finding [the arithmetic mean](https://en.wikipedia.org/wiki/Arithmetic_mean) between two numbers (_indices within the input array in this case_) and this is exactly how we will be able to calculate position of each individual element within the resulting binary search tree.

Hope these graphs are helpful enough to visualize the core idea that we will be using to build the algorithm to solve the problem.

### Building Blocks
Our base building block is very simple `Node` class that represents individual nodes within the tree:

```swift
// Binary search tree node is a recursive data structure
class Node {
    public var value: Int!, left: Node?, right: Node?

    init(value: Int) {
        self.value = value
    }
}
```

And the other building block is `BinarySearchTree` class to represent the underlying data structure and its scenario-specific operations:

```swift
class BinarySearchTree {
    // our root of the tree
    public var root: Node!

    // is the tree empty?
    public func empty() -> Bool {
        return root == nil
    }
}
```

These snippets above might seem basic but that's okay for now, since we enhance these types with some very powerful things as you will see below.

### Hands-On Experience
Now, lets roll out some adhoc code snippets that walk the steps we would perform building a binary search tree reusing the input and arithmetic mean technique from the brainstorming section.

```swift
// our input
let input = [1, 2, 3]
// define the operational boundaries using arithmetic mean technique
var start = 0, end = input.count, middle = (start + end) / 2

// init root of the tree, with the value in the middle of the array
let root = Node(value: input[middle])

// compute the middle of the sub-array on the left ([0...1])
var left = (start + middle) / 2
// init left subtree with the value in the middle of the sub-array
root.left = Node(value: input[left])
// are there more items left to process on the left? nope...
left != 0 // false

// compute the middle of the sub-array on the right ([1...2])
var right = (middle + end) / 2
// init right subtree with the value in the middle of the sub-array
root.right = Node(value: input[right])
// are there more items to process on the right? nope...
right + 1 < end // false
// we are done building our binary tree

// assert our tree is built correctly
assert(expected: 1, monogram: "1L", startFrom: root) // true
assert(expected: 2, monogram: "0T", startFrom: root) // true
assert(expected: 3, monogram: "1R", startFrom: root) // true
```

As you can tell from the code above, it should be fairly easy to write the actual implementation that solves the given problem. Lets take these learnings and write something that looks more like a final solution.

### Helpers
Before we can start on the final solution snippet, we need a few more tools, `find the insertion point` of the specified value and actually `insert a new node with the specified value`.

The following code snippet is `Node`'s class extension that finds the insertion point of the specified value:

```swift
// extension of Node class to implement scenario-specific operations
extension Node {
    // finds the insertion point of the specified value
    public func findParent(n: Int) -> Node {
        // does the node belog to the left subtree?
        if n < value && left != nil {
            // recurse into the left subtree
            return left!.findParent(n: n)
        }
        // does the node belong to the right subtree?
        else if n > value && right != nil {
            // recurse into the right subtree
            return right!.findParent(n: n)  
        }
        // default to itself
        return self
    }
}
```

Next one is the code snippet of `BinarySearchTree`'s class extension that inserts a new node with the specified value:

```swift
// BinarySearchTree extension with the scenario-specific operations
extension BinarySearchTree {
    // inserts a new node with the specified value
    public func insert(n: Int) {
        // our very first node is the root
        if empty() {
            root = Node(value: n)
            return
        }
        // initialize new node
        let node = Node(value: n)
        // find the insertion point, eq. parent node
        let parent = root.findParent(n: n)
        // attach node to the corresponding subtree
        if n < parent.value {
            // extend left subtree
            parent.left = node
        } else if n > parent.value {
            // otherwise, extend the right subtree
            parent.right = node
        }
        // oops, that's an error - since the value equals to the parent
    }
}
```

And the last but not least is the code snippet of `assert` helper function that uses mnemonic codes to validate correctness of the resulting binary search tree (_imho, it is very useful for that matter_):

```swift
// Binary tree assertion helper with mnemonic notation support
// to assert position of the specified node within the tree.
func assert(expected: Int, monogram: String, startFrom: Node) -> Bool {
    var level = 0, head = startFrom, mnemonic: String = "T"
    // infinite loop
    while true {
        // terminate the loop
        if expected == head.value { break }
        // choose mnemonic for the next element
        mnemonic = expected < head.value ? "L" : "R"
        // choose which subtree to work on
        let next = mnemonic == "L" ? head.left : head.right
        // terminate the loop
        if next == nil { break }
        // switch head to the next element
        head = next!
        // adjust the level
        level += 1
    }
    // assert node attributes, numeric value and mnemonic code
    return expected == head.value && monogram == "\(level)\(mnemonic)"
}
```

### Solution
Here is the code snippet of the final solution that uses `BinarySearchTree` data structure defined and extended above:

```swift
// creates a binary search tree with minimal height
func fromSortedArray(tree: BinarySearchTree, arr: [Int], x: Int, y: Int) {
    // our base case
    if x > y { return }
    // middle of the payload on either side
    let middle = (x + y) / 2
    // insert the element
    tree.insert(n: arr[middle])
    // work on the left subtree
    fromSortedArray(tree: tree, arr: arr, x: x, y: middle - 1)
    // work on the right subtree
    fromSortedArray(tree: tree, arr: arr, x: middle + 1, y: y)
}

// prepare state
let treeOfSix = BinarySearchTree(),
    arrayOfSix = [1, 2, 3, 4, 5, 6],
    endOfSix = arrayOfSix.count - 1

// build a binary search tree from an input array
fromSortedArray(tree: treeOfSix, arr: arrayOfSix, x: 0, y: endOfSix)
```

As you might expect, this is our traditional code snippet to validate the solution using `assert` function (_as seen in **Helpers** section_):

```swift
// validate the tree
assert(expected: 1, monogram: "1L", startFrom: treeOfSix.root!)
assert(expected: 2, monogram: "2R", startFrom: treeOfSix.root!)
assert(expected: 3, monogram: "0T", startFrom: treeOfSix.root!)
assert(expected: 4, monogram: "2L", startFrom: treeOfSix.root!)
assert(expected: 5, monogram: "1R", startFrom: treeOfSix.root!)
assert(expected: 6, monogram: "2R", startFrom: treeOfSix.root!)
```

This implementation of `fromSortedArray` function has __O(N log N)__ time complexity, where __O(N)__ is the time required to enumerate the input array and __O(log N)__ is the overhead introduced by `findParent` extension function of `Node` class (_since, it takes `log N` iterations to find a node in binary search tree_).

Fortunately, there is a trick we can use to build the tree slightly faster and in the next post we will explore a way to improve performance of the existing solution.