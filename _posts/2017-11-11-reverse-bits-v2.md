---
layout: post
title: Reverse bits (v2)
---
Our [Reverse bits (v1)]({% link _posts/2017-10-27-reverse-bits-v1.md %}) gets the job done but also has some drawbacks: it uses brute-force approach and doesn't use the hint provided (a lookup table).

### Brainstorming
To take advantage of the hint provided we need to figure out what would be the use of a lookup table in this case... Since our original constraint is a 64-bit word, we could pre-compute reverse version of these 64-bit words and store these words in an array. Later we can use the array as a lookup table to quickly fetch pre-computed results.

Here is an brief example of how an array-based lookup table would look like for a few 64-bit words (_I'm using here Swift's hexadecimal notation so the bit patterns fit into the code snippet area_):

```swift
// NOTE: Swift's hexadecimal literal notation could not be used as is
// because the compiler does not default to Int(bitPattern: ...)

// hence 0x8000_0000_0000_0000 literal simply overflows the positive
// range of Swift's Integer and results into the following:

// error integer literal '9223372036854775808' overflows when stored
// into 'Int'

// for that very same reason we have this helper to convert from
// bit pattern into a signed integer representation
func µ(_ bitPattern: UInt) -> Int {
    return Int(bitPattern: bitPattern)
}

// an example of the lookup table with only 4 pre-computed words
// to illustrate the technique's inner workings
var cache: [Int] = Array(repeating: 0, count: 4)
// our 1st pre-computed word
cache[0x0000_0000_0000_0000] = µ(0x0000_0000_0000_0000)
// our 2nd pre-computed word
cache[0x0000_0000_0000_0001] = µ(0x8000_0000_0000_0000)
// our 3rd pre-computed word
cache[0x0000_0000_0000_0002] = µ(0x4000_0000_0000_0000)
// our 4th pre-computed word
cache[0x0000_0000_0000_0003] = µ(0xc000_0000_0000_0000)
```

As you can see the idea is very simple, index in the array is the 64-bit word we need to reverse and the value stored at that index is actually a reverse version of the word. Time complexity profile of each lookup is __O(1)__, since we access array elements by their index and that operation has __O(1)__ time complexity.

Armed with this lookup table it would be a breeze to compute reverse version of any 64-bit word.

However, there is one problem... a big problem... a very big problem... a whooping size problem... a __2^64-bit__ size problem.

To store a reverse version of an every possible 64-bit word we would need a lot of memory, more than any reasonably priced computer can spare today. Lets make our calculations and explore these numbers to understand size of the problem better:

```swift
// Q: How much data it takes to store an every possible 64-bit word?

// A: It takes whooping 18,446,744,073,709,552,000 bits of memory
// to store an every possible combination of 64 bits. Below is the
// byte-precision conversion table to aid readability:

//   Problem Size  |  Precision
// ==============================
//  ~2,305,843,009 |  Gigabyte
//  ~2,305,843.01  |  Terabyte
//  ~2305.8375     |  Petabyte

// Here is the similar conversion table of my laptop's memory capacity
// in contrast to the problem size:

//  Memory Size |  Precision
// ==============================
//     16       |  Gigabyte
//    0.016     |  Terabyte
//   0.000016   |  Petabyte

// Damn... won't fit into my laptop's memory :-(
```

Well, due to the very prohibitive size of the original idea to store an every 64-bit word in an array we need to come up with a more creative solution... How about downsizing the problem and __split an entire 64-bit word into 4 separate segments, 16-bit each__.

Now, to store all possible combinations of a 16-bit word we would need much less memory, to be exact it would require us to spare only `65,536 (2^16)` bits of memory.

Here is a sample of an array-based 16-bit lookup table for the first 4 words. Note, we now use Swift's binary literal notation `0b` since at 16-bit precision binary literals are way more readable than their hexadecimal counterparts:

```swift
// an example of an array-based lookup table with the first
// 4 pre-computed 16-bit words to illustrate the technique's
// inner workings
var cache: [Int] = Array(repeating: 0, count: 65_536)
// our 1st pre-computed word
cache[0b0000_0000_0000_0000] = 0b0000_0000_0000_0000
// our 2nd pre-computed word
cache[0b0000_0000_0000_0001] = 0b1000_0000_0000_0000
// our 3rd pre-computed word
cache[0b0000_0000_0000_0010] = 0b0100_0000_0000_0000
// our 4th pre-computed word
cache[0b0000_0000_0000_0011] = 0b1100_0000_0000_0000
```

That lookup table fits very well into memory and to use it in the computation we will __split__ our 64-bit word into __4 segments__ 16-bit each, swap each segment with its reverse version and then shuffle resulting segments between each other to form a reverse version of the original 64-bit word.

### Proof of Concept
Lets work on a proof of concept and manually perform each step of the computation:
```swift
// sample lookup table with only two pre-computed words to illustrate
// the technique
var cache: [Int] = Array(repeating: 0, count: 2)
cache[0b0000_0000_0000_0000] = 0b0000_0000_0000_0000
cache[0b0000_0000_0000_0001] = 0b1000_0000_0000_0000

// here is our 64-bit input and 16-bit mask to extract each
// segment individually
let input = 0x1, mask = 0b1111_1111_1111_1111
// fetch 1st segment, bits 0...15
var _1st = input & mask,         // 0b0000_0000_0000_0001
// fetch 2nd segment, bits 16...31
    _2nd = (input >> 16) & mask, // 0b0000_0000_0000_0000
// fetch 3rd segment, bits 32...47
    _3rd = (input >> 32) & mask, // 0b0000_0000_0000_0000
// fetch 4th segment, bits 48...63
    _4th = (input >> 48) & mask  // 0b0000_0000_0000_0000

// now get from the lookup table reverse version of each segment
_1st  = cache[_1st] // 0b1000_0000_0000_0000
_2nd  = cache[_2nd] // 0b0000_0000_0000_0000
_3rd  = cache[_3rd] // 0b0000_0000_0000_0000
_4th  = cache[_4th] // 0b0000_0000_0000_0000

// our original 64-bit word layout in terms of segments looks like:

// _4th | _3rd | _2nd | _1st

// and our goal is to swap segments between each other and make them
// look like:

// _1st | _2nd | _3rd | _4th

// using the reverse version of each 16-bit segment.

// swap segments to get _1st | _2nd | _3rd | _4th layout
let reverse = (_1st << 48) | (_2nd << 32) | (_3rd << 16) | (_4th << 0)

// validate, since reverse of 0x1 is µ(0x8000_0000_0000_0000)
reverse == µ(0x8000_0000_0000_0000) // true
```
With all these findings we can implement `computeCache()` function that computes __16-bit lookup table__ and `reverse()` function that implement __problem solution__.

### 16-bit Lookup Table
Here is our first core component, computation of 16-bit lookup table with as many explanations as possible:
```swift
// An internal 4-bit cache to speed up lookup table computation
let cx: [Int] = [0b0000, 0b1000, 0b0100, 0b1100,
                 0b0010, 0b1010, 0b0110, 0b1110,
                 0b0001, 0b1001, 0b0101, 0b1101,
                 0b0011, 0b1011, 0b0111, 0b1111]

// Simple 4-bit word cache lookup (based on pre-computed values)
// NOTE: We use 0b1111 to safeguard the computation's boundaries
func xv(_ bits: Int) -> Int { return cx[bits & 0b1111] }

// INSIGHT: Look closely at pre-computed values of cx's array, you
// should notice an interesting pattern in their bits. Items at the
// opposite locations of the range are the exact opposites of each
// other:
//
//  - 0b0000 at [0] equals to NOT(0b1111) at [15];
//  - 0b1000 at [1] equals to NOT(0b0111) at [14];
//  - 0b0100 at [2] equals to NOT(0b1011) at [13];
//  ... and etc ...
//
// That insight actualy leads to an interesting technique that
// enables us to compute two items at the same time and fill out
// the lookup table 2x times faster!

// To speed up the calculation we exploit the fact that the range
// is well-known and follows predictable pattern. Bit patterns on
// the left and right are exact opposites of each other, therefore
// we can speed up lookup table calculation by factor of 2.
public func computeCache() -> [Int] {
    // declare our lookup table
    var lookup: [Int] = Array(repeating: 0, count: 65_536)
    // coordinates within the lookup table
    var x = 0, y = 65_535
    // loop thru the range and fill the table
    while x < y {
        // compute parts of the word, 4 in total
        let va = xv(x)       << 12, // move to the left by 12
            vs = xv(x >> 4)  << 8,  // move to the left by 8
            vd = xv(x >> 8)  << 4,  // move to the left by 4
            vf = xv(x >> 12) << 0   // no need to move anything
        // compute & cache value of X
        lookup[x] = va | vs | vd | vf
        // compute & cache value of Y, which is the exact opposite X
        // use mask since we need only first 16-bits
        lookup[y] = (~lookup[x]) & 0xffff
        // advance both coordinates
        x += 1;
        y -= 1;
    }
    // here is our fully computed lookup table
    return lookup
}
```
As you can easily guess, this code snippet has performance profile of `O(N/2)`. Why? Here is why, our cache size is `~65K` items and we assign it letter `N` and to compute the cache we must touch at least an every item in the cache, therefore the initial performance profile is `O(N)`. However, we were able to cut the problem size in half by computing two values at the same time, therefore we spend `2x` less time to touch all the items and hence the resulting performance profile would be `O(N/2)`.

### Problem Solution
Here is the code snippet for the problem solution that builds on top of the core component, __16-bit lookup table__:
```swift
// Prepare lookup table (16-bit cache of pre-computed bit patterns)
let cache = computeCache()

// Actual solution to the original problem using lookup table
func reverse(_ bits: Int) -> Int {
    // 16-bit mask and individual parts of 64-bit word, 4 in total
    let mask = 0xffff,
        va = cache[bits & mask] << 48,         // compute 1st part
        vs = cache[(bits >> 16) & mask] << 32, // compute 2nd part
        vd = cache[(bits >> 32) & mask] << 16, // compute 3rd part
        vf = cache[(bits >> 48) & mask]        // compute 4th part
    // Simply OR the parts computed and that is our result
    return (va | vs | vd | vf)
}

// NOTE: Since we deal with bit patterns that surpass positive range
// of signed integer on a spot, we extend use of our µ helper to both
// left and right sides of every validation case to keep the code
// somewhat readable and traceable.

// Base validations for the boundary (symmetrical) cases
reverse(µ(0x0000_0000_0000_0000)) == µ(0x0000_0000_0000_0000) // true
reverse(µ(0xffff_ffff_ffff_ffff)) == µ(0xffff_ffff_ffff_ffff) // true
reverse(µ(0xffff_0000_0000_ffff)) == µ(0xffff_0000_0000_ffff) // true
reverse(µ(0x0000_ffff_ffff_0000)) == µ(0x0000_ffff_ffff_0000) // true
reverse(µ(0x8000_0000_0000_0001)) == µ(0x8000_0000_0000_0001) // true
reverse(µ(0x8800_0000_0000_0011)) == µ(0x8800_0000_0000_0011) // true
reverse(µ(0x8888_8888_1111_1111)) == µ(0x8888_8888_1111_1111) // true

// More validations for other symmetrical and not-so cases
reverse(µ(0x0000_0000_0000_0001)) == µ(0x8000_0000_0000_0000) // true
reverse(µ(0x0000_0000_0000_ffff)) == µ(0xffff_0000_0000_0000) // true
reverse(µ(0x0000_0000_ffff_ffff)) == µ(0xffff_ffff_0000_0000) // true
reverse(µ(0x0000_0000_ffff_0000)) == µ(0x0000_ffff_0000_0000) // true
reverse(µ(0x0000_ffff_0000_0000)) == µ(0x0000_0000_ffff_0000) // true
reverse(µ(0x0000_0000_0000_1100)) == µ(0x0088_0000_0000_0000) // true
reverse(µ(0x0000_0000_0000_5000)) == µ(0x000a_0000_0000_0000) // true
reverse(µ(0x0000_0000_0005_5000)) == µ(0x000a_a000_0000_0000) // true
reverse(µ(0x0000_0000_0000_9000)) == µ(0x0009_0000_0000_0000) // true
```

What is the performance profile for `reverse()` function implemented above? It is `O(N/L)` where `N` is the number of bits in a word and `L` is the number of bits of the lookup table keys.