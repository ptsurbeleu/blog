---
layout: post
title: Computing the parity of a word (v2)
---

The [previous (brute-force) version]({% link _posts/2017-08-21-index.md %}) of the code snippet has some concerns,
lets see how we can improve it to perform the computation slightly faster.

### Compute parity (slightly faster)
There is a slightly faster way to perform the very same computation with a technique erasing the lowest set bit in a word using a single operation:

{% highlight swift %}

// erasing the lowest bit set technique for 0b0101_0001
// x &= (x - 1)

// iteration 0
0b0101_0001 - 1           == 0b0101_0000 // x - 1
0b0101_0001 & 0b0101_0000 == 0b0101_0000 // x & (x - 1)

// iteration 1
0b0101_0000 - 1           == 0b0100_1111 // x - 1
0b0101_0000 & 0b0100_1111 == 0b0100_0000 // x & (x - 1)

// iteration 2
0b0100_0000 - 1           == 0b0011_1111 // x - 1
0b0100_0000 & 0b0011_1111 == 0b0000_0000 // x & (x - 1)

// no more iterations, since the last operation yields 0

{% endhighlight %}

Here is the actual code snippet using the technique described above:

{% highlight swift %}

// we use Int as suggested by Swift user manual
func parity_v2(x: Int) -> Int {
    // overflow should not be a problem here
    var result = 0, word = x
    // loop until we examined all bits
    while word != 0 {
        // odd or even? since 1 ^ 1 == 0, 0 ^ 1 == 1
        result ^= 1
        // erase the lowest set bit
        word &= (word - 1)
    }
    // odd or even?
    return result
}

// validate the function works as advertised
parity_v2(x: 0b0000) == 0 // true
parity_v2(x: 0b0010) == 1 // true
parity_v2(x: 0b0011) == 0 // true

{% endhighlight %}

This version has slighlty improved time complexity from _O(n)_ to _O(k)_, where _k_ is the number of bits set in the word (for example, _0b0101_0100, k = 3_).

Here are the remaining concerns:
 - Still some looping is required, can we do even better?
 - We didn't use the hint provided