---
title: Compute X/Y using +, - and bitwise shift operators
layout: post
---
### Problem statement
Write a program that computes quotient of two positive integers __X__ and __Y__. Keep in mind your code needs to meet the following constraints:
  - use only `addition`, `subtraction` and `bitwise shift` operators;

```
  EXAMPLE:

  Input: x = 12, y = 4

  Output: 3
```

### Solution
Here is the actual code that implements a solution to divide __X__ by __Y__ using __addition, subtraction and bitwise operators__ only:
```swift
func divide(x: Int, y: Int) -> Int {
    // Prepare state of the algorithm
    var quotient = 0, power = 32
    // Since function parameters in Swift are constants,
    // we declare internal variables that we can modify
    var xx = x, yy = y << power
    // Iterate while X still "divisible" by Y
    while xx >= y {
        // Little speed trick, find 2^k * Y, that is less or equal to X
        while yy > xx {
            // Shift bits in 2^k * Y to the right, eq. divide by 2
            yy >>= 1
            // Try the lesser power, eq. 2^k-1
            power -= 1
        }
        // Compute parts of the quotient, bit by bit
        quotient += 1 << power
        // Subtract 2^k * Y from X
        xx -= yy
    }
    // Here is the result of division
    return quotient
}
```

And below is the traditional section with assertions to validate the code performs as advertised and establish a baseline for any improvement work in the future:
```swift
// Assert a few very basic use cases
divide(x: 0, y: 1) == 0 / 1
divide(x: 1, y: 1) == 1 / 1
divide(x: 6, y: 3) == 6 / 3
divide(x: 5, y: 1) == 5 / 1

// Assert a few slightly larger use cases
divide(x: 10, y: 1) == 10 / 1
divide(x: 10, y: 2) == 10 / 2
divide(x: 12, y: 4) == 12 / 4
divide(x: 14, y: 2) == 14 / 2

// Assert a few interesting use cases
divide(x: 13, y: 2) == 13 / 2
divide(x: 17, y: 2) == 17 / 2
divide(x: 19, y: 2) == 19 / 2
divide(x: 21, y: 2) == 21 / 2

// Assert a few edge use cases
divide(x: Int.max, y: 0x0000_ffff) == Int.max / 0x0000_ffff
divide(x: Int.max, y: 0x000f_ffff) == Int.max / 0x000f_ffff
divide(x: Int.max, y: 0x7fff_ffff) == Int.max / 0x7fff_ffff
```

### Bites & pieces
This coding challenge uses a very straightforward approach to find the quotient and an extra trick to improve performance of the program, that is to compute __the largest power__ of `2` that satisfies `2 ^ k * Y ≤ X` inequality.

Lets get started!

First of all, the code set `quotient` to `0` and declares __the largest power__ of `2` to be `32` by default (_eq. the program supports 32-bit integers only_):
```swift
// Prepare state of the algorithm
var quotient = 0, power = 32
```

Next step, is to make a local copy of `X` so it can be modified later and calculate the value of `2 ^ 32 * Y`. That value will be used later to make comparisons with the local copy of `X` and find out what is the actual __largest power__ of `2` that satisfies the inequality:
```swift
// Since function parameters in Swift are constants,
// we declare internal variables that we can modify
var xx = x, yy = y << power
```

Now, everything is ready to begin iterate on the local copy of `X` while it is still larger than `Y`, which means there is still some work to do to find `quotient`:
```swift
// Iterate while X still "divisible" by Y
while xx >= y {
  ... inner block of code ...
}
```

Inside the main loop, to improve performance of finding __the largest power__ of `2`, the trick is to use `bitwise right shift (>>)` operator on `yy` and that is equivalent of dividing it by 2 on every iteration. Also `power` is decreased by `1` each time, so once the inner loop has ended `power` would have the right number:
```swift
// Little speed trick, find 2^k * Y, that is less or equal to X
while yy > xx {
    // Shift bits in 2^k * Y to the right, eq. divide by 2
    yy >>= 1
    // Try (k - 1) power, eq. 2^k-1
    power -= 1
}
```

As soon as __the largest power__ of `2` is calculated, now it is possible to find out one of the `quotient`'s bits by moving `1` into `power`'s place using `bitwise left shift (<<)` operator and add it to the `quotient`'s bits discovered already:
```swift
// Compute parts of the quotient, bit by bit
quotient += 1 << power
```

Since it is allowed to use __subtraction__, the local copy of `X` at this step should have `2 ^ k` subtracted from it. That makes it possible for the algorithm to do the remaining work and discover other bits of the `quotient` as well:
```swift
// Subtract 2^k * Y from X
xx -= yy
```

Now once the main loop has ended, `quotient` variable used inside the main loop should have by now all the bits of the correct answer:
```swift
// Here is the result of division
return quotient
```

That should be it! 🍯

Here is a Swift playground for this article at Github: [Compute X/Y using +, - and bitwise shift operators](http://bit.ly/2NA13oo).