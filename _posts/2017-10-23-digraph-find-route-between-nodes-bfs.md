---
layout: post
title: Find route between nodes in graph (BFS)
---
### Problem Statement
Implement a function to find out whether there is a route between two nodes
in a directed graph (aka. digraph). Nodes in the graph are represented by
their integer values.

Here is our directed graph interface (_using adjacency list representation_):

```swift
class DirectedGraph {
  // ctor
  init(numberOfVertices: Int)
  // adds an edge between vertices in graph
  addEdge(v: Int, w: Int)
  // gets adjacent vertices or empty array
  adj_vertices(v: Int) -> [Int]
  // marks the given vertice as visited
  visit(v: Int)
  // find out whether the vertice is visited or not
  visited(v: Int) -> Bool
}
```

Here is the function definition:

```swift
func routable(graph: DirectedGraph, x: Int, y: Int) -> Bool {
  // implement me...
}
```

### Solution
To find out whether there is a route between nodes in a graph we can use
either **breadth first search** or **depth first search** graph traversal
technique. We begin traversing the graph at node `x` and work our way through
all adjacent nodes of `x` and their adjacent nodes. Along the way, we verify
whether the value of nodes we encounter equals `y` and when it does that means
we just found out the route from `x` to `y`.

Generally speaking, the implementation is not complicated but do requires to
properly track state of already visited nodes to avoid cycles and repetition.

### Breadth First Search
We will start with this technique to explore the implementation-specific details
and nuisances of the subject. One of the benefits of this technique is that we
inspect the immediate children nodes first and only then move on to inspect other
nodes.

With the help of very simple data structure, such as **queue**, we keep track of
the nodes to inspect and we enqueue `x` node to begin the inspection of the graph.
Following that, we dequeue `x` from the queue and enumerate its immediate children
while at the same time comparing each child node with `y` and breaking out of the
loop as soon as child node value matches `y`. However, in case child node's value
doesn't match `y` we enqeue that child node for further inspection with an assumption
that one of its child nodes (if any) would match `y`'s value.

This approach works thru the graph either finding the node matching
`y`'s value and returning `true` or visiting all nodes in the graph without finding
any node matching `y`'s value and returning `false`.

Here is the hands-on experience of the algorithm to solve the problem:

```swift
func routable(graph: DirectedGraph, x: Int, y: Int) -> Bool {
    // storage to keep track of nodes to be visited
    let queue = Queue<Int>()
    // start with our first node x
    queue.enqueue(item: x)
    // fetch cursor to iterate on
    while let cursor = queue.dequeue() {
        // enumerate all adjacent vertices
        for n in graph.adj_vertices(v: cursor) {
            // skip node we already visited
            if graph.visited(v: n) { continue }
            // yay, we have found a route between nodes!
            if n == y { return true }
            // keep looking, thus enqueue the node
            queue.enqueue(item: n)
        }
        // mark our cursor node as visited
        graph.visit(v: cursor)
    }
    // no path has been found between nodes
    return false
}
```

To to validate the implementation above, we will be using the `withEdges`
helper utility. Currently it is limited to 50 vertices but feel free to
tailor it according to your needs:

```swift
// Our helper to build directed graphs from the specified payload
func withEdges(_ edges: (Int, Int)...) -> DirectedGraph {
    // TODO: Count the actual number of vertices
    let graph = DirectedGraph(numberOfVertices: 50)
    for (x, y) in edges {
        graph.addEdge(v: x, w: y)
    }
    return graph
}
```

Finally, here is the section with a very simple validation of our
`routable` function:

```swift
// very simple graph definition, 2 -> 0 -> 1
let graphOne = withEdges((2,0), (0,1))

// validation
routable(graph: graphOne, x: 2, y: 1) == true // true
```

Hope you will find this article helpful!