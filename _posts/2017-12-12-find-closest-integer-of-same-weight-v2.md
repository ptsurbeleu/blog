---
layout: post
title: Find a closest integer with the same weight (v2)
---
In the [previous post]({% link _posts/2017-11-23-find-closest-integer-of-same-weight.md %}) on the subject of finding the closest number with the same weight, we looked at the brute-force technique that does the work but poorly performs on certain inputs.

We should evaluate an idea of using the `least significant bit` with an intent to improve the runtime performance and offset the downsides of the brute-force technique mentioned above.

### Brainstorming
Lets take a few very basic examples of the following numbers: `0b0010`, `0b0100`, `0b1010` and `0b1100`. For these cases, we can perform trivial swap operation on the least significant bit (aka. __LSB__) to get correct answer.

Here are the examples describing manual swap of the least significant bit for these numbers (_notice, the closest number is less than the original number_):

```swift
// swap bits using bitmasks, 0b0010 and 0b0001
0b0010 ^ (0b0010 | 0b0001) == 0b0001 // true
// swap bits using bitmasks, 0b0100 and 0b0010
0b0100 ^ (0b0100 | 0b0010) == 0b0010 // true
// swap bits using bitmasks, 0b0010 and 0b0001
0b1010 ^ (0b0010 | 0b0001) == 0b1001 // true
// swap bits using bitmasks, 0b0100 and 0b0010
0b1100 ^ (0b0100 | 0b0010) == 0b1010 // true
```

Even though these examples are trivial, swapping the least significant bit technique actually computes the answer elegantly and efficiently. However, what if we throw at it a few more numbers with somewhat provocative bit patterns (_notice, the closest number is greater than the original number_):

```swift
// swap bits using bitmasks, 0b0010 and 0b0001
0b0001 ^ (0b0010 | 0b0001) == 0b0010 // true
// swap bits using bitmasks, 0b1000 and 0b0100
0b0111 ^ (0b1000 | 0b0100) == 0b1011 // true
// swap bits using bitmasks, 0b0010 and 0b0001
0b1101 ^ (0b0010 | 0b0001) == 0b1110 // true
// swap bits using bitmasks, 0b0010 and 0b0001
0b0101 ^ (0b0010 | 0b0001) == 0b0110 // true
```

Yay, this is very impressive! Could not be more happier being able to find a well-working technique from the first try.

### More experiments
The only thing that is not crystal clear yet, how to compute a valid bitmask for two distinct cases (_smaller and larger closest number with the same weight_)? Lets describe how that would work using a simple case of finding the answer for two numbers `0b0010` and `0b0001`.

In order to get the answer for the first number `0b0010` we would perform the following steps:

```swift
// number to experiment with and positions of bits to swap
let num = 0b0010, x = 0, y = 1
// compute bitmask (0b0011) to swap the bits using XOR
let bitmask = (1 << y) | (1 << x)
// swap bits in positions x and y using bitmask to find the answer
0b0010 ^ bitmask == 0b0001 // true
```

Now, lets try to get the answer for the second number `0b0001` and discover the steps involved:

```swift
// number to experiment with and positions of bits to swap
let num = 0b0001, x = 0, y = 1
// compute bitmask (0b0011) to swap the bits using XOR
let bitmask = (1 << y) | (1 << x)
// swap bits in positions x and y using bitmask to find the answer
num ^ bitmask == 0b0010 // true
```

After comparing these two snippets side-by-side, it is clear that nothing have really changed between these two examples above. We used different number but the same bit positions to swap, same bitmask and in both cases we got the answer that is correct.

If the technique still not quite crystal clear to you, the next example should make it clear. Lets take another number `0b0111` and run an experiment, trying to make it just as close to the actual code in our solution as possible:

```swift
// number to experiment with and first pair of bits (0 and 1)
let num = 0b0111; var x = 0, y = 1;
// should we swap these bits? no, because they are the same
(num >> x) & 1 == (num >> y) & 1 // true
// move on to the next pair of bits (1 and 2)
x += 1; y += 1;
// should we swap these bits? again no, they are the same
(num >> x) & 1 == (num >> y) & 1 // true
// move on to the next pair of bits (2 and 3)
x += 1; y += 1;
// should we swap these bits? yes, since they are the different
(num >> x) & 1 == (num >> y) & 1 // false
// compute bitmask (0b1100) to swap the bits (2 and 3) using XOR
let bitmask = (1 << y) | (1 << x)
// swap bits in positions x and y using bitmask to find the answer
num ^ bitmask == 0b1011 // true
```

That example outlines the details of all steps involved into computing correct answer for that number. Now, knowing about these details, we should be able to put together the final solution that is faster and works for any number.

### Solution
Here is the final solution that includes these useful discoveries from the experiments concluded above:

```swift
/// error definitions for boundary cases
enum Failure : Error { case AllZeroes; case AllOnes; }

/// extracts the specified bit from the given number
func bit(_ num: Int, _ i: Int) -> Int {
    return (num >> i) & 1
}

/// finds the closest number with the same weight
func find_closest(num: Int) throws -> Int {
    // enumerate all bits (0 through 63) in the given number
    for i in 0..<64 {
        // extract bits in pairs and compare them
        if bit(num, i) != bit(num, i + 1) {
            // swap bits that are different using XOR and bitmask
            return num ^ ((1 << i) | 1 << (i + 1))
        }
    }
    // case of all zeroes is an error 
    if num == 0  { throw Failure.AllZeroes }
    // so is, the case of all ones
    throw Failure.AllOnes
}
```

Below is the section with validations, but this time validations represented by two distinct parts: _smaller and larger numbers_.

Here is the first part validating the solution against smaller numbers in `binary` notation (_for readability_):

```swift
// the closest to 0b00110 (6) is 0b00101 (5)
try find_closest(num: 0b00110) == 0b00101 // true
// the closest to 0b01101 (13) is 0b01110 (14)
try find_closest(num: 0b01101) == 0b01110 // true
// the closest to 0b11000 (24) is 0b10100 (20)
try find_closest(num: 0b11000) == 0b10100 // true
// the closest to 0b10000 (16) is 0b01000 (8)
try find_closest(num: 0b10000) == 0b01000 // true
```

Following is the second part validating the solution against larger numbers in `hexadecimal` notation (_again, for readability_):

```swift
// the closest to 0x2000 (8192) is 0x1000 (4096)
try find_closest(num: 0x2000) == 0x1000 // true
// the closest to 0x4000 (16384) is 0x2000 (8192)
try find_closest(num: 0x4000) == 0x2000 // true
// the closest to 0x8000 (32768) is 0x4000 (16384)
try find_closest(num: 0x8000) == 0x4000
```

As we can tell by looking at the code above, it would have to inspect all the bits in the worst case which brings us to time complexity of `O(n)` and `O(1)` space complexity, since we do not allocate any extra space to compute the results.

Hope you will find the article userful!