---
layout: post
title: Highlights of Jenkins World 2016
---
The [conference](http://bit.ly/2cVzF4q) was fully packed with practically valuable talks, great speakers and inspirational experiences. I enjoyed a lot talking to Jenkins first-class experts at Open Source Hub and [CloudBees](http://bit.ly/2cVA5ro) main booth. Special kudos to all CloudBees folks at the event for taking time to answer questions, share their expertise and personal takeaways. All questions about Jenkins and infrastructure that I had were answered at the speed of thought.

The conference was too large to see it all, so here is what happened for me at the conference:

- Worked my way thru [Intro to Plugin Development Workshop](http://bit.ly/2cVy8vc) by [Steven Christou](http://bit.ly/2cVyule);
- [Thinking Inside the Container: A Continuous Delivery Story](http://bit.ly/2dk6m9T) by [Maxfield F Stewart](https://twitter.com/maxfields2000) from [RiotGames](http://riot.com/2dk66Ym);
- Discovered an amazing open-source tool [Taurus](http://bit.ly/2diXyRG) developed by folks at [BlazeMeter.com](http://bit.ly/2cSGpQA);
- Found out about amazing open-source Azure plugins for Jenkins from [Azure DevOps Integrations with Jenkins](http://bit.ly/2dk6AOk) by [Damien Caro](http://bit.ly/2cVymCA) and [Arun Chandrasekhar](http://bit.ly/2dlOkE4);
- Learned great lessons about Jenkins.io from [Continuous Delivery of Infrastructure with Jenkins](http://bit.ly/2dk7zhr) by [R. Tyler Croy](https://twitter.com/jenkinsci);
- Got very excited about the upcoming Jenkins refresh with [Blue Ocean](http://bit.ly/2cVA72k) from [Blue Ocean: A New User Experience for Jenkins](http://bit.ly/2dk77j2) by [James Dumay](http://bit.ly/2cVyiT1);
- Figured it out that [Pipelines](http://bit.ly/2cVzN3P) is the future of Jenkins;

I have been very fortunate to meet all these wonderful people at the conference. CloudBees folks did an amazing job organizing the venue, sessions, food, swag giveaways and so much more.

It was a great time spent in San Jose, CA. I also ended up reading the entire book [Extending Jenkins](http://amzn.to/2dkat5F) by Donald Simpson during my time at the conference and learned a good bit of the Jenkins internals already.
