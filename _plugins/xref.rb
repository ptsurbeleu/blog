require "liquid"

module Jekyll
  # CrossReferenceTag is a simple helper to arrange cross-references in a single document.
  class CrossReferenceTag < Liquid::Tag
      def initialize(tag_name, input, tokens)
        super
        # NOTE: ...
        parts = input.split ","
        @text, @ref = parts[0], parts[1]
      end

      def render(context)
        if @text and @ref then
          "<a href=\"##{@ref.strip}\">#{@text.strip}</a>"
        else
          "<i id=\"#{@text.strip}\"></i>"
        end
      end
  end
end

Liquid::Template.register_tag('xref', Jekyll::CrossReferenceTag)